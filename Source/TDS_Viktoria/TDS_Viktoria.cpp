// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_Viktoria.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_Viktoria, "TDS_Viktoria" );

DEFINE_LOG_CATEGORY(LogTDS_Viktoria)
DEFINE_LOG_CATEGORY(LogTDS_Net)
 