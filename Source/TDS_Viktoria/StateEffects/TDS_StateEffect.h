// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TDS_StateEffect.generated.h"
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDestroyed);
/**
 * 
 */
 /////////////////////////////////////////Base////////////////////////////////////////////////////
UCLASS(Blueprintable, BlueprintType)
class TDS_VIKTORIA_API UTDS_StateEffect : public UObject
{
    GENERATED_BODY()

protected:

    AActor* AffectedActor = nullptr;

public:
    virtual bool IsSupportedForNetworking() const override { return true; };
    virtual bool InitObject(AActor* Actor, FName NameBoneHit);
    virtual void DestroyObject();
    FOnDestroyed OnDestroyed;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    UTexture2D* EffectIcon;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    bool bStackable = false;

    virtual float GetDuration();

    UPROPERTY(Replicated)
    FName NameBone;
};

/////////////////////////////////////////ExecuteOnce////////////////////////////////////////////////////
UCLASS()
class TDS_VIKTORIA_API UTDS_StateEffect_ExecuteOnce : public UTDS_StateEffect
{
    GENERATED_BODY()

public:
    bool InitObject(AActor* Actor, FName NameBoneHit) override;
    void DestroyObject() override;

    virtual void InitialExecute();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
    float Power = 20.0f;

    /// FX
    //
    // This effect CANT be looped
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Initial Execute FX")
    class UNiagaraSystem* InitialEffect = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Initial Execute FX")
    FVector InitialEffectOffset = FVector::ZeroVector;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Initial Execute FX")
    FVector InitialEffectScale = FVector(1.0f, 1.0f, 1.0f);

    // This sound CANT be looped
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Initial Execute FX")
    USoundBase* InitialSound = nullptr;

};

/////////////////////////////////////////WithDuration////////////////////////////////////////////////////
UCLASS()
class TDS_VIKTORIA_API UTDS_StateEffect_WithDuration : public UTDS_StateEffect_ExecuteOnce
{
    GENERATED_BODY()

public:
    bool InitObject(AActor* Actor, FName NameBoneHit) override;
    void DestroyObject() override;
    float GetDuration() override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting With Duration")
    float EffectTime = 5.0f;

    /// FX
    //
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Duration FX")
    class UNiagaraSystem* DurationEffect = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Duration FX")
    FVector DurationEffectOffset = FVector::ZeroVector;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Duration FX")
    FVector DurationEffectScale = FVector(1.0f, 1.0f, 1.0f);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Duration FX")
    USoundBase* DurationSound = nullptr;

    //class UNiagaraComponent* DurationNiagaraComponent = nullptr;
    class UAudioComponent* DurationAudioComponent = nullptr;

    FTimerHandle TimerHandle_EffectTimer;
};

/////////////////////////////////////////ExecuteTimer////////////////////////////////////////////////////
UCLASS()
class TDS_VIKTORIA_API UTDS_StateEffect_ExecuteTimer : public UTDS_StateEffect_WithDuration
{
    GENERATED_BODY()

public:
    bool InitObject(AActor* Actor, FName NameBoneHit) override;
    void DestroyObject() override;

    virtual void Execute();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
    float ExecuteTime = 1.0f;

    FTimerHandle TimerHandle_ExecuteTimer;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////SUBCLASSES////////////////////////////////////////////////////
// 
/////////////////////////////////ExecuteTimer_HealthInfluence//////////////////////////////////////////
UCLASS()
class TDS_VIKTORIA_API UTDS_StateEffect_ExecuteTimer_HealthInfluence : public UTDS_StateEffect_ExecuteTimer
{
    GENERATED_BODY()

public:
    void Execute() override;
};

/////////////////////////////////ExecuteOnce_HealthInfluence//////////////////////////////////////////
UCLASS()
class TDS_VIKTORIA_API UTDS_StateEffect_ExecuteOnce_HealthInfluence : public UTDS_StateEffect_ExecuteOnce
{
    GENERATED_BODY()

public:
    void InitialExecute() override;
};

/////////////////////////////////WithDuration_DamageCoef//////////////////////////////////////////
UCLASS()
class TDS_VIKTORIA_API UTDS_StateEffect_WithDuration_DamageCoef : public UTDS_StateEffect_WithDuration
{
    GENERATED_BODY()

public:
    void InitialExecute() override;
    void DestroyObject() override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Damage Coef")
    float newDamageCoef = 0.5f;

    float ActorsDamageCoef;
};

/////////////////////////////////ExecuteTimer_Aura//////////////////////////////////////////
UCLASS()
class TDS_VIKTORIA_API UTDS_StateEffect_ExecuteTimer_Aura : public UTDS_StateEffect_ExecuteTimer
{
    GENERATED_BODY()

public:
    void Execute() override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Aura")
    float AuraRadius = 200.0f;

    /// Affected by aura outer Actors FX
    //
    // This effect CANT be looped
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aura effect FX")
    class UNiagaraSystem* AuraAffectedEffect = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aura effect FX")
    FVector AuraAffectedEffectOffset = FVector::ZeroVector;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aura effect FX")
    FVector AuraAffectedEffectScale = FVector(1.0f, 1.0f, 1.0f);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aura effect FX")
    USoundBase* AuraAffectedSound = nullptr;
};

/////////////////////////////////WithDuration_Stun//////////////////////////////////////////
UCLASS()
class TDS_VIKTORIA_API UTDS_StateEffect_WithDuration_Stun : public UTDS_StateEffect_WithDuration
{
    GENERATED_BODY()

public:
    void InitialExecute() override;
    void DestroyObject() override;
};
