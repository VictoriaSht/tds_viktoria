// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_StateEffect.h"
#include "TDS_Viktoria/Character/TDSHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Particles/ParticleSystemComponent.h"
#include "TDS_Viktoria/Interface/TDS_IGameActor.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Components/AudioComponent.h"
#include "Net/UnrealNetwork.h"

/////////////////////////////////////////Base////////////////////////////////////////////////////

bool UTDS_StateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	if (Actor)
	{
		AffectedActor = Actor;
		ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(AffectedActor);
		if (myInterface)
		{
			myInterface->AddEffect(this, GetDuration());
		}
	}
	NameBone = NameBoneHit;

	return true;
}

void UTDS_StateEffect::DestroyObject()
{
	if (AffectedActor)
	{
		ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(AffectedActor);
		if (myInterface)
		{
			myInterface->RemoveEffect(this);
		}
	}
	AffectedActor = nullptr;
	if (this && IsValidLowLevel())
	{
		ConditionalBeginDestroy();
	}

	OnDestroyed.Broadcast();

}

float UTDS_StateEffect::GetDuration()
{
	return -1.0f;
}

void UTDS_StateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTDS_StateEffect, NameBone);
}

/////////////////////////////////////////ExecuteOnce////////////////////////////////////////////////////

bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	if (Super::InitObject(Actor, NameBoneHit))
	{
		if (InitialEffect)
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), InitialEffect, AffectedActor->GetActorLocation() + InitialEffectOffset, FRotator::ZeroRotator, InitialEffectScale, true, true);
		if (InitialSound)
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), InitialSound, AffectedActor->GetActorLocation());

		InitialExecute();
		return true;
	}
	return false;
}

void UTDS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteOnce::InitialExecute()
{
}

/////////////////////////////////////////WithDuration////////////////////////////////////////////////////

bool UTDS_StateEffect_WithDuration::InitObject(AActor* Actor, FName NameBoneHit)
{
	if (Super::InitObject(Actor, NameBoneHit))
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_WithDuration::DestroyObject, EffectTime);
		if (AffectedActor)
		{
			FVector Loc(0);

			/*if (DurationEffect)
			{
				USceneComponent* MyMesh = Cast<USceneComponent>(AffectedActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
				if (MyMesh)
					DurationNiagaraComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(DurationEffect, MyMesh, NameBoneHit, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				else
					DurationNiagaraComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(DurationEffect, AffectedActor->GetRootComponent(), NameBoneHit, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}*/
			if (DurationSound)
				DurationAudioComponent = UGameplayStatics::SpawnSoundAttached(DurationSound, AffectedActor->GetRootComponent(), NameBoneHit, Loc, FRotator(0), EAttachLocation::SnapToTarget);

		}
		return true;
	}
	return false;
}

void UTDS_StateEffect_WithDuration::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}

	//if (DurationNiagaraComponent)
	//{
	//	DurationNiagaraComponent->DestroyComponent();
	//	DurationNiagaraComponent = nullptr;
	//}
	if (DurationAudioComponent)
	{
		DurationAudioComponent->FadeOut(0.5f, 0);
		DurationAudioComponent->DestroyComponent();
		DurationAudioComponent = nullptr;
	}

	Super::DestroyObject();
}

float UTDS_StateEffect_WithDuration::GetDuration()
{
	return EffectTime;
}

/////////////////////////////////////////ExecuteTimer////////////////////////////////////////////////////

bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	if (Super::InitObject(Actor, NameBoneHit))
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_StateEffect_ExecuteTimer::Execute, ExecuteTime, true);
		Execute();
		return true;
	}

	return false;
}

void UTDS_StateEffect_ExecuteTimer::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteTimer::Execute()
{
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////SUBCLASSES////////////////////////////////////////////////////
/////////////////////////////////ExecuteTimer_HealthInfluence//////////////////////////////////////////

void UTDS_StateEffect_ExecuteTimer_HealthInfluence::Execute()
{
	if (AffectedActor)
	{
		UTDSHealthComponent* myHealthComponent = Cast<UTDSHealthComponent>(AffectedActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHealthComponent)
		{
			if (Power > 0)
				myHealthComponent->IncreaseHealth_OnServer(Power);
			else if (Power < 0)
				myHealthComponent->DecreaseHealth_OnServer(-Power);
		}
	}
	else
		DestroyObject();
}

/////////////////////////////////ExecuteOnce_HealthInfluence//////////////////////////////////////////

void UTDS_StateEffect_ExecuteOnce_HealthInfluence::InitialExecute()
{
	if (AffectedActor)
	{
		UTDSHealthComponent* myHealthComponent = Cast<UTDSHealthComponent>(AffectedActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHealthComponent)
		{
			if (Power > 0)
				myHealthComponent->IncreaseHealth_OnServer(Power);
			else if (Power < 0)
				myHealthComponent->DecreaseHealth_OnServer(-Power);
		}
	}

	DestroyObject();
}

/////////////////////////////////WithDuration_invulnerability//////////////////////////////////////////

void UTDS_StateEffect_WithDuration_DamageCoef::InitialExecute()
{
	if (AffectedActor)
	{
		UTDSHealthComponent* myHealthComponent = Cast<UTDSHealthComponent>(AffectedActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHealthComponent)
		{
			ActorsDamageCoef = myHealthComponent->DamageCoef;
			myHealthComponent->SetDamageCoef(newDamageCoef);
		}
	}

}

void UTDS_StateEffect_WithDuration_DamageCoef::DestroyObject()
{
	if (AffectedActor)
	{
		UTDSHealthComponent* myHealthComponent = Cast<UTDSHealthComponent>(AffectedActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHealthComponent)
		{
			myHealthComponent->SetDamageCoef(ActorsDamageCoef);
		}
	}
	Super::DestroyObject();
}

/////////////////////////////////ExecuteTimer_Aura//////////////////////////////////////////

void UTDS_StateEffect_ExecuteTimer_Aura::Execute()
{
	if (AffectedActor)
	{
		TArray<AActor*> ActorsToIgnore;
		ActorsToIgnore.Add(AffectedActor);

		TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
		ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_PhysicsBody));
		ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));
		ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldDynamic));

		//TArray<FHitResult> HitResults;
		//UKismetSystemLibrary::SphereTraceMulti(GetWorld(), AffectedActor->GetActorLocation(), AffectedActor->GetActorLocation(), 
		//	AuraRadius, ETraceTypeQuery::TraceTypeQuery1, false, ActorsToIgnore, EDrawDebugTrace::ForDuration, HitResults, true);
		TArray<AActor*> DetectedActors;

		UKismetSystemLibrary::SphereOverlapActors(GetWorld(), AffectedActor->GetActorLocation(), AuraRadius, ObjectTypes, AActor::StaticClass(), ActorsToIgnore, DetectedActors);

		for (AActor* elem : DetectedActors)
		{
			UTDSHealthComponent* myHealthComponent = Cast<UTDSHealthComponent>(elem->GetComponentByClass(UTDSHealthComponent::StaticClass()));
			if (myHealthComponent)
			{
				if (AuraAffectedEffect)
					UNiagaraFunctionLibrary::SpawnSystemAttached(AuraAffectedEffect, elem->GetRootComponent(), FName(NAME_None), FVector::ZeroVector, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, true);
				if (AuraAffectedSound)
					UGameplayStatics::SpawnSoundAtLocation(GetWorld(), AuraAffectedSound, elem->GetActorLocation());

				if (Power > 0)
					myHealthComponent->IncreaseHealth_OnServer(Power);
				else if (Power < 0)
					myHealthComponent->DecreaseHealth_OnServer(-Power);
			}
		}
	}
}

/////////////////////////////////WithDuration_Stun//////////////////////////////////////////

void UTDS_StateEffect_WithDuration_Stun::InitialExecute()
{
	if (AffectedActor)
	{
		ITDS_IGameActor* myGameActor = Cast<ITDS_IGameActor>(AffectedActor);
		if (myGameActor)
		{
			myGameActor->OnStunned();
		}
	}
}

void UTDS_StateEffect_WithDuration_Stun::DestroyObject()
{
	if (AffectedActor)
	{
		ITDS_IGameActor* myGameActor = Cast<ITDS_IGameActor>(AffectedActor);
		if (myGameActor)
		{
			myGameActor->StunEnd();
		}
	}

	Super::DestroyObject();
}