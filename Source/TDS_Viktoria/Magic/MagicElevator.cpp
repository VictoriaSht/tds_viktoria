// Fill out your copyright notice in the Description page of Project Settings.


#include "MagicElevator.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

// Components
#include "Components/BoxComponent.h"
#include "Components/WidgetComponent.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Sound/SoundWave.h"
#include "Components/AudioComponent.h"

// Sets default values
AMagicElevator::AMagicElevator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Creating mesh...
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = Mesh;

	// Creating MagicCastZone-trigger...
	MagicCastZone = CreateDefaultSubobject<UBoxComponent>(TEXT("MagicCastZone"));
	MagicCastZone->SetupAttachment(Mesh);
	//MagicCastZone->SetCollisionResponseToAllChannels(ECR_Ignore);
	//MagicCastZone->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);

	// Creating Spell VFX...
	MagicVFX = CreateDefaultSubobject<UNiagaraComponent>(TEXT("MagicVFX"));
	MagicVFX->SetActive(false);
	MagicVFX->SetupAttachment(Mesh);

	// Creating widget...
	MagicWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("MagicWidget"));
	MagicWidget->SetVisibility(false);
	MagicWidget->SetupAttachment(Mesh);

	/** 
		Setting defaults 
	*/

	// Spell
	bActivated = false;
	bGoingUp = false;
	SpellAudioComp = nullptr;

	// Going Up and Down
	OpenTime = 0.5;
	ZMax = 750.f;
	ZOssfetCurrent = 0.f;


}

// Called when the game starts or when spawned
void AMagicElevator::BeginPlay()
{
	Super::BeginPlay();
	ZInitial = GetActorLocation().Z;

	// Bind event to overlap
	MagicCastZone->OnComponentBeginOverlap.AddDynamic(this, &AMagicElevator::ActorBeginOverlap);
	
}

// Called every frame
void AMagicElevator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMagicElevator::SetEnabled(bool bEnabled)
{
	bEnabledForActivation = bEnabled;

	AActor* Player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (IsOverlappingActor(Player))
	{
		Activate();
	}
}

void AMagicElevator::ActorBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (bEnabledForActivation)
	{
		// If not activated, activate: put on ice material and enable magic spell
		if (OtherActor == UGameplayStatics::GetPlayerPawn(GetWorld(), 0) && !bActivated)
		{
			Activate();
		}
	}
}

void AMagicElevator::SpellCast()
{
	if (bActivated)
	{
		// Start going up
		bGoingUp = true;
		GetWorldTimerManager().SetTimer(UpDownTimer, this, &AMagicElevator::UpDownStep, 0.01, true);

		// Visual and audio
		MagicVFX->Activate();
		SpellAudioComp = UGameplayStatics::SpawnSound2D(GetWorld(), SpellSound);
		SpellAudioComp->FadeIn(1.f, 1.f);
		MagicWidget->SetVisibility(false);
	}

}

void AMagicElevator::UpDownStep()
{
	// If function returns true, moving is completed
	if (AddSubtractOffset(bGoingUp))
		GetWorldTimerManager().PauseTimer(UpDownTimer);
}

bool AMagicElevator::AddSubtractOffset(bool bAdd)
{
	if (bAdd)
	{
		// Calculate offset +
		float NewProbableOffset = ZOssfetCurrent + (ZMax / (OpenTime / 0.01));
		bool Done = (NewProbableOffset >= ZMax);
		ZOssfetCurrent = UKismetMathLibrary::SelectFloat(ZMax, NewProbableOffset, Done);

		// Move Actor
		SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, ZInitial + ZOssfetCurrent));

		return Done;
	}
	else
	{
		// Calculate offset -
		float NewProbableOffset = ZOssfetCurrent - (ZMax / (OpenTime / 0.01));
		bool Done = (NewProbableOffset <= 0.f);
		ZOssfetCurrent = UKismetMathLibrary::SelectFloat(0.f, NewProbableOffset, Done);

		// Move Actor
		SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, ZInitial + ZOssfetCurrent));

		return Done;
	}

	return false;
}

void AMagicElevator::Activate()
{
	Mesh->SetMaterial(0, IceMaterial);
	MagicWidget->SetVisibility(true);
	bActivated = true;
}

void AMagicElevator::SpellEndCasting()
{
	if (bActivated)
	{
		// Visual and audio
		MagicVFX->Deactivate();
		SpellAudioComp->FadeOut(1.f, 0.f);

		// Wait some time before going down
		GetWorldTimerManager().SetTimer(SpellTimer, this, &AMagicElevator::SpellEnded, 3.f);
	}
}

void AMagicElevator::SpellEnded()
{
	MagicWidget->SetVisibility(true);

	//Start going down
	bGoingUp = false;
	GetWorldTimerManager().SetTimer(UpDownTimer, this, &AMagicElevator::UpDownStep, 0.01, true);
}

