// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "MagicInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UMagicInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_VIKTORIA_API IMagicInterface
{
	GENERATED_BODY()

public:

	virtual void SpellCast();
	virtual void SpellEndCasting();

};
