// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MagicInterface.h"
#include "MagicElevator.generated.h"

UCLASS()
class TDS_VIKTORIA_API AMagicElevator : public AActor, public IMagicInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMagicElevator();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* Going up and down */
	bool bGoingUp;
	FTimerHandle UpDownTimer;
	void UpDownStep();
	bool AddSubtractOffset(bool bAdd);
	float OpenTime;
	float ZMax;
	float ZOssfetCurrent;
	float ZInitial;	

	// For activation
	bool bActivated;

	// Activation function
	void Activate();

	// For enabling
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MagicObject")
	bool bEnabledForActivation = false;

	// Components
	UPROPERTY(VisibleAnywhere, Category = Components);
	UStaticMeshComponent* Mesh;
	UPROPERTY(VisibleAnywhere, Category = Components);
	class UNiagaraComponent* MagicVFX;
	UPROPERTY(VisibleAnywhere, Category = Components)
	class UBoxComponent* MagicCastZone;
	UPROPERTY(VisibleAnywhere, Category = Components);
	class UWidgetComponent* MagicWidget;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "MagicObject")
	void SetEnabled(bool bEnabled);

	// Assets
	UPROPERTY(BlueprintReadWrite, EditAnywhere);
	UMaterialInstance* IceMaterial;
	UPROPERTY(BlueprintReadWrite, EditAnywhere);
	class USoundWave* SpellSound;

	// Actor begin overlap
	UFUNCTION()
	void ActorBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/* Spell */
	class UAudioComponent* SpellAudioComp;
	void SpellEnded();
	FTimerHandle SpellTimer;

	/* Magic Interface */
	void SpellCast() override;
	void SpellEndCasting() override;

};
