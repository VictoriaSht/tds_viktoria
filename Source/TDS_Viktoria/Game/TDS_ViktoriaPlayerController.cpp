// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_ViktoriaPlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "TDS_Viktoria/Character/TDS_ViktoriaCharacter.h"
#include "Engine/World.h"
#include "InputActionValue.h"
#include "Engine/LocalPlayer.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

ATDS_ViktoriaPlayerController::ATDS_ViktoriaPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
}

void ATDS_ViktoriaPlayerController::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

}

void ATDS_ViktoriaPlayerController::OnUnPossess()
{
	// Call the base class 
	Super::OnUnPossess();


}
