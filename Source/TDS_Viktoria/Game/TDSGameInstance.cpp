// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameInstance.h"

bool UTDSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFound = false;
	if (WeaponInfoTable)
	{
		FWeaponInfo* WeaponInfoRow;
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);

		if (WeaponInfoRow)
		{
			bIsFound = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));

	return bIsFound;
}

bool UTDSGameInstance::GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo)
{
	bool bIsFound = false;
	if (DropItemTable)
	{
		FDropItem* DromItemInfoRow;
		DromItemInfoRow = DropItemTable->FindRow<FDropItem>(NameItem, "", false);

		if (DromItemInfoRow)
		{
			bIsFound = true;
			OutInfo = *DromItemInfoRow;
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetWeaponInfoByName - DropItemTableRow -NULL"));
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetWeaponInfoByName - DropItemTable -NULL"));

	return bIsFound;
}
