// Fill out your copyright notice in the Description page of Project Settings.


#include "GlobalTypes.h"
#include "TDS_Viktoria/StateEffects/TDS_StateEffect.h"
#include "TDS_Viktoria/Interface/TDS_IGameActor.h"
#include "Perception/AISense_Damage.h"

void UGlobalTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UTDS_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (AddEffectClass && TakeEffectActor && SurfaceType)
	{
		UTDS_StateEffect* myEffect = Cast<UTDS_StateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsCanAdd = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsCanAdd)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsCanAdd = true;
					bool bStackCheckPass = true;
					if (!myEffect->bStackable)
					{
						ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(TakeEffectActor);
						if (myInterface)
						{
							TArray<UTDS_StateEffect*> Effects = myInterface->GetCurrentEffects();
							int8 elemEffect = 0;
							bool bIsFound = false;
							while (elemEffect < Effects.Num() && !bIsFound)
							{
								UClass* EffectClass = Effects[elemEffect]->GetClass();
								if (myEffect->GetClass() == EffectClass)
								{
									bStackCheckPass = false;
									bIsFound = true;
								}
								elemEffect++;
							}
						}
					}

					if (bStackCheckPass)
					{
						UTDS_StateEffect* NewEffect = NewObject<UTDS_StateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor, NameBoneHit);
						}
					}
				}
				i++;
			}
		}
	}
}

void UGlobalTypes::DamageImpact(UObject* WorldContextObject, float DamageAmount, AActor* Instigator, AActor* inDamagedActor, FHitResult Hit, TSubclassOf<UTDS_StateEffect> AddEffectClass)
{
	EPhysicalSurface mySurface;
	ITDS_IGameActor* DamagedActor = Cast<ITDS_IGameActor>(inDamagedActor);
	if (DamagedActor)
	{
		if (AddEffectClass && DamagedActor->GetSurfaceType(mySurface))
			UGlobalTypes::AddEffectBySurfaceType(inDamagedActor, Hit.BoneName, AddEffectClass, mySurface);
	}

	UAISense_Damage::ReportDamageEvent(WorldContextObject, inDamagedActor, Instigator, DamageAmount, Hit.Location, Hit.Location);
}
