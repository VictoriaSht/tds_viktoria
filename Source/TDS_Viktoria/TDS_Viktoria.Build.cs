// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS_Viktoria : ModuleRules
{
	public TDS_Viktoria(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "NavigationSystem", "AIModule", "Niagara", "EnhancedInput", "UMG", "PhysicsCore", "Slate" });
    }
}
