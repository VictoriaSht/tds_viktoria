// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetSystemLibrary.h"
#include "TDS_Viktoria/StateEffects/TDS_StateEffect.h"
#include "TDS_Viktoria/Interface/TDS_IGameActor.h"

// Debug
int32 bShowExplosionDebug = 0;
FAutoConsoleVariableRef CVAREShowExplosionDebug(
	TEXT("TDS.ExplosionShow"),
	bShowExplosionDebug,
	TEXT("TDS.ExplosionShow"),
	ECVF_Cheat);

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplosion(DeltaTime);
}

void AProjectileDefault_Grenade::TimerExplosion(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplode > TimeToExplode)
		{
			Explosion();
		}
		else
			TimerToExplode += DeltaTime;
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComponent, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	if (ProjectileSetting.TimeToExplode > 0)
	{
		TimeToExplode = ProjectileSetting.TimeToExplode;
		TimerEnabled = true;
	}
	else
		Explosion();
}

void AProjectileDefault_Grenade::Explosion()
{
	if (bShowExplosionDebug)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Green, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage / 2, 12, FColor::Orange, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Red, false, 12.0f);
	}

	TimerEnabled = false;
	if (ProjectileSetting.ExplodeFX)
		FxExplosion_Multicast(ProjectileSetting.ExplodeFX);
	if (ProjectileSetting.ExplodeSound)
		SoundExplosion_Multicast(ProjectileSetting.ExplodeSound);

	TArray<AActor*> IgnoredActors;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExplodeMaxDamage,
		ProjectileSetting.ExplodeMinDamage,
		GetActorLocation(),
		ProjectileSetting.ProjectileMaxRadiusDamage,
		ProjectileSetting.ProjectileMaxRadiusDamage,
		ProjectileSetting.ExplodeDamageFalloff,
		NULL, IgnoredActors, this, nullptr);

	TArray<FHitResult> OverlappedActors;
	UKismetSystemLibrary::SphereTraceMulti(GetWorld(), GetActorLocation(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 
											ETraceTypeQuery::TraceTypeQuery1, false, IgnoredActors, EDrawDebugTrace::None, OverlappedActors, true);

	for (auto elem : OverlappedActors)
	{
		UGlobalTypes::DamageImpact(GetWorld(), 20, GetInstigator(), elem.GetActor(), elem, ProjectileSetting.Effect);
		// ToDo Damage Amount
	}

	this->Destroy();
}

void AProjectileDefault_Grenade::SoundExplosion_Multicast_Implementation(USoundBase* Sound)
{
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), Sound, GetActorLocation());
}

void AProjectileDefault_Grenade::FxExplosion_Multicast_Implementation(UParticleSystem* FxTemplate)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxTemplate, GetActorLocation(), GetActorRotation(), FVector(1));
}
