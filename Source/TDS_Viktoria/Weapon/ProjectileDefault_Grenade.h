// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class TDS_VIKTORIA_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	
public:
	virtual void Tick(float DeltaTime) override;
	
	void TimerExplosion(float DeltaTime);

	virtual void BulletCollisionSphereHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	virtual void ImpactProjectile() override;

	void Explosion();

	bool TimerEnabled = false;
	float TimerToExplode = 0.0f;
	float TimeToExplode = 5.0f;

	// For Debug
	bool bShowExplosionDebug = false;

	// Network
	UFUNCTION(NetMulticast, Reliable)
	void FxExplosion_Multicast(UParticleSystem* FxTemplate);
	UFUNCTION(NetMulticast, Reliable)
	void SoundExplosion_Multicast(USoundBase* Sound);
};
