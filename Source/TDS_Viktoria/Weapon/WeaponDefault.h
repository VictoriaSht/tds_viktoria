// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "TDS_Viktoria/FuncLibrary/GlobalTypes.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, AnimHip);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoTaken);

UCLASS()
class TDS_VIKTORIA_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FOnWeaponReloadStart OnWeaponReloadStart;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FWeaponInfo WeaponSetting;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAdditionalWeaponInfo AdditionalWeaponInfo;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void ClipDropTick(float DeltaTime);
	void ShellDropTick(float DeltaTime);

	// Initialize weapon mesh
	void WeaponInit();
	
	//////////////////////// Fire Logic ////////////////////

	UFUNCTION(BlueprintCallable, Category = "Debug")
	void Fire(); // fire func

	// Shoot End Location
	FVector GetFireEndLocation();
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	FVector ShootEndLocation = FVector(0);

	// Weapon State
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "FireLogic")
	void SetWeaponStateFire_OnServer(bool bIsFire);
	bool CheckWeaponCanFire();
	UFUNCTION(Server, Reliable)
	void UpdateStateWeapon_OnServer(EMovementState NewMovementState);

	// Projectile
	FProjectileInfo GetProjectile();
	int8 GetNumberProjectileByShot() const;
	
	// Flags
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool bWeaponFiring = false;
	bool bBlockFire;
	bool bWeaponAiming;

	// Dropping meshes
	UFUNCTION(Server, Reliable)
	void InitDropMesh_OnServer(UStaticMesh* DropMesh, FTransform Offset, 
		FVector DropImpulseDirection, float LifeTimeMesh,
		float ImpulseRandomDispersion, float ImpulsePower, float DropCustomMass);
	// shell
	bool bDropShellFlag = false;
	float DropShellTimer = -1.0f;
	// clip
	bool bDropClipFlag = false;
	float DropClipTimer = -1.0f;

	// Reloading
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Reload Logic")
	bool bWeaponReloading = false;
	UFUNCTION(BlueprintCallable, Category = "Reload Logic")
	int32 GetWeaponRound();
	void InitReload();
	void FinishReload();
	void CancelReload();
	bool CheckWeaponCanReload();
	int32 GetReloadingAmmoAvailable();
	int32 GetCurrentAmmoFromInventory();

	// Dispersion
	void ChangeDispersionByShot();
	float GetCurrentDispersion() const;
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;
	bool bShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	// Timers
	float FireTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reload Logic")
	float ReloadTimer = 0.0f;

	// Debug
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeShootDirectionLogic = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reload Logic")
	float ReloadTime = 0.0f;

	// Network
	UFUNCTION(Server, Unreliable)
	void UpdateWeaponByCharacterMovementState_OnServer(FVector NewShootEndLocation, bool newShouldReduceDispersion);

	UFUNCTION(NetMulticast, Unreliable)
	void AnimWeaponFire_Multicast(UAnimMontage* AnimFire);
	UFUNCTION(NetMulticast, Unreliable)
	void ShellDropFire_Multicast(UStaticMesh* DropMesh, FTransform Transform, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float ImpulsePower, float DropCustomMass, FVector LocalDir);
	
	UFUNCTION(NetMulticast, Unreliable)
	void FXWeaponFire_Multicast(UParticleSystem* FireFxTemplate, USoundBase* FireSound);

	UFUNCTION(NetMulticast, Reliable)
	void TraceFireSpawnHitDecal_Multicast(UMaterialInterface* DecalMaterial, FHitResult HitResult);
	UFUNCTION(NetMulticast, Reliable)
	void TraceFireSpawnHitFX_Multicast(UParticleSystem* FxTemplate, FHitResult HitResult);
	UFUNCTION(NetMulticast, Reliable)
	void TraceFireSpawnHitSound_Multicast(USoundBase* HitSound, FHitResult HitResult);
};
