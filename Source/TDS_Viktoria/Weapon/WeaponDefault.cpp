// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "ProjectileDefault.h"
#include "Engine/StaticMeshActor.h"
#include "TDS_Viktoria/Character/TDSInventoryComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TDS_Viktoria/Interface/TDS_IGameActor.h"
#include "Net/UnrealNetwork.h"

int32 ShowDebug = 0;
FAutoConsoleVariableRef CVarWeaponShow(
	TEXT("TDS.DebugWeapon"),
	ShowDebug,
	TEXT("Draw Debug For Weapon"),
	ECVF_Cheat);

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Creating root component...
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	// Creating skeletal mesh...
	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("No Collision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	// Creating static mesh...
	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("No Collision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	// Creating shoot location arrow...
	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Shoot Location"));
	ShootLocation->SetupAttachment(RootComponent);

	// Network
	bReplicates = true;
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	
	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (HasAuthority())
	{
		FireTick(DeltaTime);
		ReloadTick(DeltaTime);
		DispersionTick(DeltaTime);
		ClipDropTick(DeltaTime);
		ShellDropTick(DeltaTime);
	}
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (bWeaponFiring && GetWeaponRound() > 0 && !bWeaponReloading)
	{
		if (FireTimer < 0.f)
		{
			if (!bWeaponReloading)
			{
				Fire();
			}
		}
		else
			FireTimer -= DeltaTime;
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (bWeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!bWeaponReloading)
	{
		if (!bWeaponFiring)
		{
			if (bShouldReduceDispersion)
			{
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
			}
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else if (CurrentDispersion > CurrentDispersionMax)
		{
			CurrentDispersion = CurrentDispersionMax;
		}
	}
	if (ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponDefault::ClipDropTick(float DeltaTime)
{
	if (bDropClipFlag)
	{
		if (DropClipTimer < 0.0f)
		{
			bDropClipFlag = false;
			InitDropMesh_OnServer(WeaponSetting.ClipDropMesh.DropMesh,
				WeaponSetting.ClipDropMesh.DropMeshOffset,
				WeaponSetting.ClipDropMesh.DropMeshImpulseDir,
				WeaponSetting.ClipDropMesh.DropMeshLifeTime,
				WeaponSetting.ClipDropMesh.ImpulseRandomDispersion,
				WeaponSetting.ClipDropMesh.PowerImpulse,
				WeaponSetting.ClipDropMesh.CustomMass);
		}
		else
			DropClipTimer -= DeltaTime;
	}
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if (bDropShellFlag)
	{
		if (DropShellTimer < 0.0f)
		{
			bDropShellFlag = false;
			InitDropMesh_OnServer(WeaponSetting.ShellBullets.DropMesh,
				WeaponSetting.ShellBullets.DropMeshOffset,
				WeaponSetting.ShellBullets.DropMeshImpulseDir,
				WeaponSetting.ShellBullets.DropMeshLifeTime,
				WeaponSetting.ShellBullets.ImpulseRandomDispersion,
				WeaponSetting.ShellBullets.PowerImpulse,
				WeaponSetting.ShellBullets.CustomMass);
		}
		else
			DropShellTimer -= DeltaTime;
	}
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->GetSkinnedAsset())
		SkeletalMeshWeapon->DestroyComponent();

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
		StaticMeshWeapon->DestroyComponent();
}

int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	// On Server

	bWeaponReloading = true;

	ReloadTimer = WeaponSetting.ReloadTime;

	UAnimMontage* AnimToPlay = nullptr;
	if (bWeaponAiming)
		AnimToPlay = WeaponSetting.AnimationInfo.AnimCharReload;
	else
		AnimToPlay = WeaponSetting.AnimationInfo.AnimCharReloadAim;
	OnWeaponReloadStart.Broadcast(AnimToPlay);

	UAnimMontage* AnimWeaponToPlay = nullptr;
	if (bWeaponAiming)
		AnimWeaponToPlay = WeaponSetting.AnimationInfo.AnimWeaponReloadAim;
	else
		AnimWeaponToPlay = WeaponSetting.AnimationInfo.AnimWeaponReload;

	if (AnimWeaponToPlay
		&& SkeletalMeshWeapon
		&& SkeletalMeshWeapon->HasValidAnimationInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(AnimWeaponToPlay);
	}

	if (WeaponSetting.ClipDropMesh.DropMesh)
	{
		bDropClipFlag = true;
		DropClipTimer = WeaponSetting.ClipDropMesh.DropMeshTime;
	}
}

void AWeaponDefault::FinishReload()
{
	bWeaponReloading = false;
	
	int32 AmmoTaken = GetReloadingAmmoAvailable();
	AdditionalWeaponInfo.Round = AdditionalWeaponInfo.Round + AmmoTaken;
	
	OnWeaponReloadEnd.Broadcast(true, AmmoTaken);
}

void AWeaponDefault::CancelReload()
{
	bWeaponReloading = false;

	if (SkeletalMeshWeapon && SkeletalMeshWeapon->HasValidAnimationInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);

	OnWeaponReloadEnd.Broadcast(false, 0);
	bDropClipFlag = false;
	DropClipTimer = -1.0f;
}

bool AWeaponDefault::CheckWeaponCanReload()
{
	bool result = true;

	// Check if weapon is reloading
	if (!bWeaponReloading)
	{
		if (GetCurrentAmmoFromInventory() <= 0)
			result = false;
	}
	else
		result = false;

	return result;
}

int32 AWeaponDefault::GetReloadingAmmoAvailable()
{
	int32 CurrentAmmoInv = GetCurrentAmmoFromInventory();
	int32 NewAmmo = WeaponSetting.MaxRound - GetWeaponRound();
	if (CurrentAmmoInv < NewAmmo)
		NewAmmo = CurrentAmmoInv;

	return NewAmmo;
}

int32 AWeaponDefault::GetCurrentAmmoFromInventory()
{
	int32 result = 0;

	if (GetOwner())
	{
		UTDSInventoryComponent* myInventory = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if (myInventory)
		{
			result = myInventory->GetAmmoForWeapon(WeaponSetting.WeaponType);
		}
	}
	return result;
}

void AWeaponDefault::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
	if (CheckWeaponCanFire())
		bWeaponFiring = bIsFire;
	else
		bWeaponFiring = false;
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !bBlockFire;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

void AWeaponDefault::InitDropMesh_OnServer_Implementation(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float ImpulsePower, float DropCustomMass)
{
	if (DropMesh)
	{
		FTransform Transform;

		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X + this->GetActorRightVector() * Offset.GetLocation().Y + this->GetActorUpVector() * Offset.GetLocation().Z;

		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());
		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());

		ShellDropFire_Multicast(DropMesh, Transform, DropImpulseDirection, LifeTimeMesh, ImpulseRandomDispersion, ImpulsePower, DropCustomMass, LocalDir);
	}
}

void AWeaponDefault::Fire()
{
	// On Server by bWeaponFiring

	// Drop Meshes
	if (WeaponSetting.ShellBullets.DropMesh)
	{
		if (WeaponSetting.ShellBullets.DropMeshTime < 0.0f)
		{
			InitDropMesh_OnServer(WeaponSetting.ShellBullets.DropMesh, 
				WeaponSetting.ShellBullets.DropMeshOffset, 
				WeaponSetting.ShellBullets.DropMeshImpulseDir, 
				WeaponSetting.ShellBullets.DropMeshLifeTime, 
				WeaponSetting.ShellBullets.ImpulseRandomDispersion, 
				WeaponSetting.ShellBullets.PowerImpulse, 
				WeaponSetting.ShellBullets.CustomMass);
		}
		else
		{
			bDropShellFlag = true;
			DropShellTimer = WeaponSetting.ShellBullets.DropMeshTime;
		}
	}

	// Sound and FX
	FXWeaponFire_Multicast(WeaponSetting.EffectFireWeapon, WeaponSetting.SoundFireWeapon);

	// Fire Logic
	FireTimer = WeaponSetting.RateOfFire;
	AdditionalWeaponInfo.Round = AdditionalWeaponInfo.Round - 1;
	ChangeDispersionByShot();
	int8 NumberProjectile = GetNumberProjectileByShot();

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo = GetProjectile();
		
		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; i++)
		{
			EndLocation = GetFireEndLocation();
			

			if (ProjectileInfo.Projectile)
			{
				// Projectile init ballistic fire

				FVector Dir = EndLocation - SpawnLocation;
				Dir.Normalize();

				FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(ProjectileInfo);

				}
			}
			else
			{
				// Hitscan

				FHitResult Hit;
				TArray<AActor*> Actors;

				
				//DrawDebugLine(GetWorld(), SpawnLocation, EndLocation.GetClampedToMaxSize(WeaponSetting.DinstanceTrace), FColor::Blue, false, 12.0f);
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("%f %f %f"), (EndLocation * WeaponSetting.DinstanceTrace).X, (EndLocation * WeaponSetting.DinstanceTrace).Y, (EndLocation * WeaponSetting.DinstanceTrace).Z));
				//FVector v = EndLocation.GetClampedToMaxSize(WeaponSetting.DinstanceTrace);
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Vector: %f %f %f"), v.X, v.Y, v.Z));
				
				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation,
					ETraceTypeQuery::TraceTypeQuery4, false, Actors, EDrawDebugTrace::None, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);
				if (ShowDebug)
					DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector()*WeaponSetting.DinstanceTrace, FColor::Black, false, 5.0f, (uint8)'\000', 0.5f);

				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					EPhysicalSurface mySurfaceType = UGameplayStatics::GetSurfaceType(Hit);
					if (mySurfaceType)
					{
						if (WeaponSetting.ProjectileSetting.HitDecals.Contains(mySurfaceType))
						{
							UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[mySurfaceType];

							if (myMaterial && Hit.GetComponent())
							{
								TraceFireSpawnHitDecal_Multicast(myMaterial, Hit);
							}

						}
						if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurfaceType))
						{
							UParticleSystem* myParticleSystem = WeaponSetting.ProjectileSetting.HitFXs[mySurfaceType];
							if (myParticleSystem)
								TraceFireSpawnHitFX_Multicast(myParticleSystem, Hit);
						}

						if (WeaponSetting.ProjectileSetting.HitSounds.Contains(mySurfaceType))
						{
							USoundBase* mySound = WeaponSetting.ProjectileSetting.HitSounds[mySurfaceType];
							if (mySound)
								TraceFireSpawnHitSound_Multicast(mySound, Hit);
						}
						else
						{
							USoundBase* mySound = WeaponSetting.ProjectileSetting.HitSounds[EPhysicalSurface::SurfaceType_Default];
							if (mySound)
								TraceFireSpawnHitSound_Multicast(mySound, Hit);
						}

						UGameplayStatics::ApplyPointDamage(Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, Hit.TraceStart, Hit, GetInstigatorController(), this, NULL);
						UGlobalTypes::DamageImpact(GetWorld(), WeaponSetting.ProjectileSetting.ProjectileDamage, GetInstigator(), Hit.GetActor(), Hit, WeaponSetting.ProjectileSetting.Effect);
					}
				}
			}
		}
	}

	// Animation 
	UAnimMontage* AnimToPlay = nullptr;
	if (bWeaponAiming)
		AnimToPlay = WeaponSetting.AnimationInfo.AnimCharFireAim;
	else
		AnimToPlay = WeaponSetting.AnimationInfo.AnimCharFire;
	OnWeaponFireStart.Broadcast(AnimToPlay);

	if (WeaponSetting.AnimationInfo.AnimWeaponFire)
		AnimWeaponFire_Multicast(WeaponSetting.AnimationInfo.AnimWeaponFire);

	// Reloading
	if (GetWeaponRound() <= 0 && !bWeaponReloading)
	{
		if (CheckWeaponCanReload())
			InitReload();
	}

}

void AWeaponDefault::UpdateStateWeapon_OnServer_Implementation(EMovementState NewMovementState)
{
	bBlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:

		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimReduction;
		bWeaponAiming = true;
		break;
	case EMovementState::AimWalk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimReduction;
		bWeaponAiming = true;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimReduction;
		bWeaponAiming = false;
		break;
	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Run_StateDispersionAimReduction;
		bWeaponAiming = false;
		break;
	case EMovementState::SprintRun_State:
		bWeaponAiming = false;
		bBlockFire = true;
		SetWeaponStateFire_OnServer(false); // set fire trigger to false
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	return CurrentDispersion;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

void AWeaponDefault::FXWeaponFire_Multicast_Implementation(UParticleSystem* FireFxTemplate, USoundBase* FireSound)
{
	if (FireSound)
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), FireSound, ShootLocation->GetComponentLocation());
	if (FireFxTemplate)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FireFxTemplate, ShootLocation->GetComponentTransform());
}

void AWeaponDefault::ShellDropFire_Multicast_Implementation(UStaticMesh* DropMesh, FTransform Transform, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float ImpulsePower, float DropCustomMass, FVector LocalDir)
{
	FActorSpawnParameters Params;
	Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	Params.Owner = this;

	AStaticMeshActor* NewActor = nullptr;
	NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Params);

	if (NewActor && NewActor->GetStaticMeshComponent())
	{
		NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
		NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		NewActor->SetActorTickEnabled(false);
		NewActor->InitialLifeSpan = LifeTimeMesh;
		NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
		NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
		NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

		if (DropCustomMass > 0)
			NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, DropCustomMass, true);

		if (!DropImpulseDirection.IsNearlyZero())
		{
			FVector FinalDir;
			LocalDir = LocalDir + (DropImpulseDirection * 1000.0f);

			if (!FMath::IsNearlyZero(ImpulseRandomDispersion))
				FinalDir = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDispersion);
			FinalDir.GetSafeNormal(0.0001f);

			NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir * ImpulsePower);
		}
	}
}

void AWeaponDefault::AnimWeaponFire_Multicast_Implementation(UAnimMontage* AnimFire)
{
	if (AnimFire && SkeletalMeshWeapon && SkeletalMeshWeapon->HasValidAnimationInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(AnimFire);
	}
}

void AWeaponDefault::UpdateWeaponByCharacterMovementState_OnServer_Implementation(FVector NewShootEndLocation, bool newShouldReduceDispersion)
{
	ShootEndLocation = NewShootEndLocation;
	bShouldReduceDispersion = newShouldReduceDispersion;
}

FVector AWeaponDefault::GetFireEndLocation()
{
	//bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);
	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() - (ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation)).GetSafeNormal() * 20000.0f);
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DinstanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DinstanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f,FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}

	return EndLocation;
}

void AWeaponDefault::TraceFireSpawnHitDecal_Multicast_Implementation(UMaterialInterface* DecalMaterial, FHitResult HitResult)
{
	UGameplayStatics::SpawnDecalAttached(DecalMaterial, FVector(20.0f), HitResult.GetComponent(), NAME_None, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
}

void AWeaponDefault::TraceFireSpawnHitFX_Multicast_Implementation(UParticleSystem* FxTemplate, FHitResult HitResult)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxTemplate, FTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint, FVector(1.0f)));
}

void AWeaponDefault::TraceFireSpawnHitSound_Multicast_Implementation(USoundBase* HitSound, FHitResult HitResult)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, HitResult.ImpactPoint);
}


void AWeaponDefault::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeaponDefault, AdditionalWeaponInfo);
	DOREPLIFETIME(AWeaponDefault, ShootEndLocation);
	DOREPLIFETIME(AWeaponDefault, bWeaponReloading);
}