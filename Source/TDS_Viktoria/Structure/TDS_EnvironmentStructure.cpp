// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_EnvironmentStructure.h"
#include "Engine/ActorChannel.h"
#include "TDS_Viktoria/StateEffects/TDS_StateEffect.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ATDS_EnvironmentStructure::ATDS_EnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// For replication
	SetReplicates(true);
}

// Called when the game starts or when spawned
void ATDS_EnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDS_EnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

TArray<UTDS_StateEffect*> ATDS_EnvironmentStructure::GetCurrentEffects()
{
	return Effects;
}

void ATDS_EnvironmentStructure::RemoveEffect(UTDS_StateEffect* RemovedEffect)
{
	Effects.Remove(RemovedEffect);

	SwitchEffect(RemovedEffect, false);
	EffectRemove = RemovedEffect;
}

void ATDS_EnvironmentStructure::AddEffect(UTDS_StateEffect* AddedEffect, float Duration)
{
	Effects.Add(AddedEffect);

	SwitchEffect(AddedEffect, true);
	EffectAdd = AddedEffect;
}

void ATDS_EnvironmentStructure::OnStunned()
{
}

void ATDS_EnvironmentStructure::StunEnd()
{
}

void ATDS_EnvironmentStructure::OnDead()
{
}

bool ATDS_EnvironmentStructure::GetSurfaceType(EPhysicalSurface& SurfaceType)
{
	bool result = false;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			SurfaceType = myMaterial->GetPhysicalMaterial()->SurfaceType;
			result = true;
		}
	}
	return result;
}

void ATDS_EnvironmentStructure::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void ATDS_EnvironmentStructure::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void ATDS_EnvironmentStructure::SwitchEffect(UTDS_StateEffect* Effect, bool IsAdded)
{
	UTDS_StateEffect_WithDuration* EffectTimer = Cast<UTDS_StateEffect_WithDuration>(Effect);
	if (EffectTimer && EffectTimer->DurationEffect)
	{
		if (IsAdded)
		{
			USceneComponent* mySceneComponent = GetRootComponent();
			if (mySceneComponent)
			{
				UNiagaraComponent* myParticle = UNiagaraFunctionLibrary::SpawnSystemAttached(EffectTimer->DurationEffect, mySceneComponent, Effect->NameBone, FVector(0), FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(myParticle);
			}
		}
		else
		{
			for (UNiagaraComponent* elem : ParticleSystemEffects)
			{
				if (elem->GetAsset() && EffectTimer->DurationEffect == elem->GetAsset())
				{
					elem->Deactivate();
					elem->DestroyComponent();
					break;
				}
			}
		}
	}
}

bool ATDS_EnvironmentStructure::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}

	return Wrote;
}

void ATDS_EnvironmentStructure::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATDS_EnvironmentStructure, Effects);
	DOREPLIFETIME(ATDS_EnvironmentStructure, EffectAdd);
	DOREPLIFETIME(ATDS_EnvironmentStructure, EffectRemove);
}

