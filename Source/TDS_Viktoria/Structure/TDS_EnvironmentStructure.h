// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS_Viktoria/Interface/TDS_IGameActor.h"
#include "TDS_EnvironmentStructure.generated.h"

UCLASS()
class TDS_VIKTORIA_API ATDS_EnvironmentStructure : public AActor, public ITDS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDS_EnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Replication
	bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	// Effects
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly)
	TArray<UTDS_StateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UTDS_StateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UTDS_StateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<class UNiagaraComponent*> ParticleSystemEffects;

	TArray<UTDS_StateEffect*> GetCurrentEffects() override;
	void RemoveEffect(class UTDS_StateEffect* RemovedEffect) override;
	void AddEffect(class UTDS_StateEffect* AddedEffect, float Duration = -1.0f) override;

	void OnStunned() override;
	void StunEnd() override;

	void OnDead() override;

	// From interface
	bool GetSurfaceType(EPhysicalSurface& SurfaceType) override;

	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION()
	void EffectRemove_OnRep();

	UFUNCTION()
	void SwitchEffect(UTDS_StateEffect* Effect, bool IsAdded);
};
