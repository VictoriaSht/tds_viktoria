// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyCharacterBase.h"
#include "TDS_Viktoria/StateEffects/TDS_StateEffect.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Engine/ActorChannel.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AEnemyCharacterBase::AEnemyCharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetReplicates(true);
}

// Called when the game starts or when spawned
void AEnemyCharacterBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemyCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemyCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

TArray<UTDS_StateEffect*> AEnemyCharacterBase::GetCurrentEffects()
{
	return Effects;
}

void AEnemyCharacterBase::RemoveEffect(UTDS_StateEffect* RemovedEffect)
{
	Effects.Remove(RemovedEffect);

	SwitchEffect(RemovedEffect, false);
	EffectRemove = RemovedEffect;
}

void AEnemyCharacterBase::AddEffect(UTDS_StateEffect* AddedEffect, float Duration)
{
	Effects.Add(AddedEffect);

	SwitchEffect(AddedEffect, true);
	EffectAdd = AddedEffect;
}

void AEnemyCharacterBase::OnStunned()
{
}

void AEnemyCharacterBase::StunEnd()
{
}

void AEnemyCharacterBase::OnDead()
{
}

bool AEnemyCharacterBase::GetSurfaceType(EPhysicalSurface& SurfaceType)
{
	bool result = false;
	USkeletalMeshComponent* myMesh = Cast<USkeletalMeshComponent>(GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			SurfaceType = myMaterial->GetPhysicalMaterial()->SurfaceType;
			result = true;
		}
	}
	return result;
}

void AEnemyCharacterBase::AddEffectToTarget(AActor* Target, TSubclassOf<class UTDS_StateEffect> EffectToApply)
{
	if (EffectToApply)
	{
		UTDS_StateEffect* NewEffect = NewObject<UTDS_StateEffect>(this, EffectToApply);
		if (NewEffect)
		{
			NewEffect->InitObject(Target, NAME_None);
		}
	}
}

void AEnemyCharacterBase::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void AEnemyCharacterBase::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void AEnemyCharacterBase::SwitchEffect(UTDS_StateEffect* Effect, bool IsAdded)
{
	UTDS_StateEffect_WithDuration* EffectTimer = Cast<UTDS_StateEffect_WithDuration>(Effect);
	if (EffectTimer && EffectTimer->DurationEffect)
	{
		if (IsAdded)
		{
			USceneComponent* mySceneComponent = Cast<USceneComponent>(GetComponentByClass(USkeletalMeshComponent::StaticClass()));
			if (mySceneComponent)
			{
				UNiagaraComponent* myParticle = UNiagaraFunctionLibrary::SpawnSystemAttached(EffectTimer->DurationEffect, mySceneComponent, Effect->NameBone, FVector(0), FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(myParticle);
			}
		}
		else
		{
			for (UNiagaraComponent* elem : ParticleSystemEffects)
			{
				if (elem->GetAsset() && EffectTimer->DurationEffect == elem->GetAsset())
				{
					elem->Deactivate();
					elem->DestroyComponent();
					break;
				}
			}
		}
	}
}

bool AEnemyCharacterBase::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}

	return Wrote;
}

void AEnemyCharacterBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AEnemyCharacterBase, Effects);
	DOREPLIFETIME(AEnemyCharacterBase, EffectAdd);
	DOREPLIFETIME(AEnemyCharacterBase, EffectRemove);
}


