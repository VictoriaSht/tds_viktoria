// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDS_Viktoria/FuncLibrary/GlobalTypes.h"
#include "TDS_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_VIKTORIA_API ITDS_IGameActor
{
	GENERATED_BODY()

public:

	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interface")
	//bool AvailableForEffects();

	/*  Effects */ 

	virtual void OnStunned() = 0;
	virtual void StunEnd() = 0;

	virtual bool GetSurfaceType(EPhysicalSurface& SurfaceType) = 0;
	virtual TArray<class UTDS_StateEffect*> GetCurrentEffects() = 0;
	virtual void RemoveEffect(class UTDS_StateEffect* RemovedEffect) = 0;
	virtual void AddEffect(class UTDS_StateEffect* AddedEffect, float Duration = -1.0f) = 0;

	/*  Health */

	UFUNCTION()
	virtual void OnDead();

	/*  Inventory */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropWeaponToWorld(FDropItem DropWeaponInfo, FWeaponSlot WeaponInfo);
};
