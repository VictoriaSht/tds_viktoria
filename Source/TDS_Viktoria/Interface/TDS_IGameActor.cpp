// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_IGameActor.h"
#include "TDS_Viktoria/StateEffects/TDS_StateEffect.h"

// Add default functionality here for any ITDS_IGameActor functions that are not pure virtual.

void ITDS_IGameActor::OnDead()
{
	TArray<UTDS_StateEffect*> AllCurrentEffects = GetCurrentEffects();
	for (UTDS_StateEffect* elem : AllCurrentEffects)
	{
		elem->DestroyObject();
	}
}
