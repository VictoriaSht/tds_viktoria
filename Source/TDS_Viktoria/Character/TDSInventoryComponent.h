// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDS_Viktoria/FuncLibrary/GlobalTypes.h"
#include "TDSInventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnSwitchWeapon, FName, WeaponIdName, FAdditionalWeaponInfo, WeaponAdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnAmmoSlotInfoChange, EWeaponType, TypeAmmo, int32, Count, bool, bTryReload);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlot, int32, IndexSlotChange, FWeaponSlot, NewSlotInfo);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_VIKTORIA_API UTDSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTDSInventoryComponent();

	// Player changes weapon
	FOnSwitchWeapon OnSwitchWeapon;
	// Change info in widget ammo slots
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnAmmoSlotInfoChange OnAmmoSlotInfoChange;
	// Change info in widget weapon slots
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnUpdateWeaponSlot OnUpdateWeaponSlot;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")
	TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")
	TArray<FAmmoSlot> AmmoSlots;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapons")
	int32 MaxSlotsWeapon = 0;

	bool SwitchWeaponIndexPush(int32 CurrentIndex, bool bIsForward, int32& Newindex);
	bool SwitchWeaponToIndex(int32 newIndex, int32 CurrentIndex);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Interface")
	FAdditionalWeaponInfo GetAdditionalInfoWeapon(int32 WeaponIndex);
	int32 GetWeaponIndexSlotByName(FName IdWeaponName);
	FName GetWeaponNameBySlot(int32 IndexSlot);
	void SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo);
	UFUNCTION(BlueprintCallable, Category = "Interface")
	void AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 AmmoAdded);

	int32 GetAmmoForWeapon(EWeaponType WeaponType);

	UFUNCTION(BlueprintCallable, Category = "Weapons")
	bool HaveEmptyWeaponSlots();

	// Interface PickUp Actors
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool CheckCanTakeAmmo(EWeaponType WeaponType);
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool CheckCanTakeWeapon(int32& FreeSlot, FName NewWeaponId = NAME_None);
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem &DropItemInfo, FWeaponSlot &OldWeapon);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
	void TryGetWeaponToInventory_OnServer(AActor* PickUpActor, FWeaponSlot NewWeapon);

	// Drop Item
	UFUNCTION(Server, Reliable)
	void DropWeaponByIndex_OnServer(int32 Index);
	bool GetDropItemInfoFromInventory(int32 Index, FDropItem& DropItemInfo);

	// For Blueprints
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory")
	void GetSlots(TArray<FWeaponSlot> &OutWeaponSlots, TArray<FAmmoSlot> &OutAmmoSlots);

	// Network
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
	void InitInventory_OnServer(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoSlotsInfo);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
	void AmmoChangeEvent_Multicast(EWeaponType TypeWeapon, int32 Count, bool bTryToReload);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
	void WeaponChangeEvent_Multicast(int32 SlotIndex, FWeaponSlot NewInfo);

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
	void SwitchWeaponEvent_Multicast(FName WeaponName, FAdditionalWeaponInfo WeaponAddInfo);
};
