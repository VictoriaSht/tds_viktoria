// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
//#include "TDS_Viktoria/Interface/TDS_IGameActor.h"
#include "TDSHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnHealthChange, float, Health, float, CurrentDamageCoef, float, ChangeValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDamageCoefChange, float, Health, float, NewCoef);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_VIKTORIA_API UTDSHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTDSHealthComponent();

	UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnDead OnDead;
	UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnDamageCoefChange OnDamageCoefChange;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(Replicated)
	float Health = 100.0f;

	class ITDS_IGameActor* OwnerGameActor = nullptr;

	UPROPERTY(Replicated)
	bool bIsAlive = true;

public:	
	UPROPERTY(Replicated)
	bool bPawnInControl = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float DamageCoef = 1.0f;


	UPROPERTY(BlueprintReadOnly, Category = "Health")
	AController* LastDamageInstigator;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Health")
	float GetCurrentHealth();
	UFUNCTION(BlueprintCallable, Category = "Health")
	void SetCurrentHealth(float NewHealth);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Health")
	virtual void DecreaseHealth_OnServer(float Damage);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Health")
	virtual void IncreaseHealth_OnServer(float Healing);

	void SetDamageCoef(float newCoef);

	UFUNCTION()
	void OwnerGotDamaged(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Health")
	bool GetIsAlive();

	// Network
	UFUNCTION(NetMulticast, Reliable, Category = "Health")
	void OnHealthChangeEvent_Multicast(float NewHealth, float CurrentDamageCoef, float ChangeValue);

	UFUNCTION(NetMulticast, Reliable, Category = "Health")
	void OnDamageCoefChangeEvent_Multicast(float NewHealth, float NewCoef);

	UFUNCTION(NetMulticast, Reliable, Category = "Health")
	void DeathEvent_Multicast();
};
