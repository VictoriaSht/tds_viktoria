// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSHealthComponent.h"
#include "TDS_Viktoria/Interface/TDS_IDamageCauser.h"
#include "TDS_Viktoria/FuncLibrary/GlobalTypes.h"
#include "TDS_Viktoria/Interface/TDS_IGameActor.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UTDSHealthComponent::UTDSHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// Network
	SetIsReplicatedByDefault(true);
}


// Called when the game starts
void UTDSHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UTDSHealthComponent::OwnerGotDamaged);

	OwnerGameActor = Cast<ITDS_IGameActor>(GetOwner());
	if (OwnerGameActor)
	{
		OnDead.AddDynamic(OwnerGameActor, &ITDS_IGameActor::OnDead);
	}
	
}


// Called every frame
void UTDSHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UTDSHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTDSHealthComponent::SetCurrentHealth(float NewHealth)
{
	if (Health > NewHealth)
		DecreaseHealth_OnServer(Health - NewHealth);
	else
		IncreaseHealth_OnServer(NewHealth - Health);
}

void UTDSHealthComponent::DecreaseHealth_OnServer_Implementation(float Damage)
{
	if (bIsAlive && DamageCoef != 0)
	{
		Damage *= DamageCoef; // Change damage by coef
		Health -= Damage; 	// Take Damage

		OnHealthChangeEvent_Multicast(Health, DamageCoef, -Damage / DamageCoef);
		if (Health <= 0)
		{
			//Damage += Health; // Decreace Damage by health that went under zero
			//Health = 0.0f;
			bIsAlive = false;
			bPawnInControl = false;
			DeathEvent_Multicast();
		}
	}
}

void UTDSHealthComponent::IncreaseHealth_OnServer_Implementation(float Healing)
{
	if (bIsAlive)
	{
		// Change health
		Health += Healing;

		if (Health > 100.0f)
		{
			Healing -= Health - 100.0f; // substract overhealth from Healing Done
			Health = 100.0f;
		}
	
		OnHealthChangeEvent_Multicast(Health, DamageCoef, Healing);
	}
}

void UTDSHealthComponent::SetDamageCoef(float newCoef)
{
	if (DamageCoef - newCoef != 0)
	{
		if (DamageCoef - newCoef > 0)
			Health *= newCoef;
		else
		{
			Health /= DamageCoef;
			if (Health > 100)
				Health = 100;
		}

		DamageCoef = newCoef;
		OnDamageCoefChangeEvent_Multicast(Health, DamageCoef);
	}
}

void UTDSHealthComponent::OwnerGotDamaged(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (Health > 0)
	{
		if (bIsAlive)
		{
			LastDamageInstigator = InstigatedBy;
			DecreaseHealth_OnServer(Damage);
		}
	}
}

bool UTDSHealthComponent::GetIsAlive()
{
	return bIsAlive;
}

void UTDSHealthComponent::DeathEvent_Multicast_Implementation()
{
	OnDead.Broadcast();
}

void UTDSHealthComponent::OnHealthChangeEvent_Multicast_Implementation(float NewHealth, float CurrentDamageCoef, float ChangeValue)
{
	OnHealthChange.Broadcast(NewHealth, CurrentDamageCoef, ChangeValue);
}

void UTDSHealthComponent::OnDamageCoefChangeEvent_Multicast_Implementation(float NewHealth, float NewCoef)
{
	OnDamageCoefChange.Broadcast(NewHealth, NewCoef);
}

void UTDSHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTDSHealthComponent, Health);
	DOREPLIFETIME(UTDSHealthComponent, bIsAlive);
	DOREPLIFETIME(UTDSHealthComponent, bPawnInControl);
}

