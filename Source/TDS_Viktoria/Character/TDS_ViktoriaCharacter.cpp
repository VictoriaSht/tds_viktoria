// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_ViktoriaCharacter.h"

// Components
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Custom components
#include "TDSInventoryComponent.h"
#include "TDSCharHealthComponent.h"

// Input
#include "InputMappingContext.h" 
#include "EnhancedInputSubsystems.h" 
#include "EnhancedInputComponent.h" 

//Custom classes
#include "TDS_Viktoria/Magic/MagicInterface.h"
#include "TDS_Viktoria/Game/TDSGameInstance.h"
#include "TDS_Viktoria/Game/TDS_ViktoriaPlayerController.h"
#include "TDS_Viktoria/StateEffects/TDS_StateEffect.h"

// Other
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/PlayerController.h"
#include "TDS_Viktoria/TDS_Viktoria.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

// Network
#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"


ATDS_ViktoriaCharacter::ATDS_ViktoriaCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create interact sphere...
	InteractSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Interact Sphere"));
	InteractSphere->SetupAttachment(RootComponent);
	InteractSphere->OnComponentBeginOverlap.AddDynamic(this, &ATDS_ViktoriaCharacter::InteractSphereBeginOverlap);
	InteractSphere->OnComponentEndOverlap.AddDynamic(this, &ATDS_ViktoriaCharacter::InteractSphereEndOverlap);

	// Create invetory system component...
	InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>(TEXT("Inventory Component"));
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDS_ViktoriaCharacter::InitWeapon);
		InventoryComponent->OnAmmoSlotInfoChange.AddDynamic(this, &ATDS_ViktoriaCharacter::AmmoChangeReloadInit);
	}

	// Create health and setup system component...
	HealthComponent = CreateDefaultSubobject<UTDSCharHealthComponent>(TEXT("HealthComponent"));

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	// Sprint logic
	SprintEnergy = SprintMaxEnergy;

	// NetWork
	bReplicates = true;
}

void ATDS_ViktoriaCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	MovementTick(DeltaSeconds);
	EnergyTick(DeltaSeconds);

	if (CurrentCursor && myPC && myPC->IsLocalPlayerController() && HealthComponent->bPawnInControl)
	{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
	}
}

void ATDS_ViktoriaCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (GetNetMode() != NM_DedicatedServer)
	{
		if (CursorMaterial && (GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority))
		{
			CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
		}
	}
}

void ATDS_ViktoriaCharacter::NumberInputPressed(const FInputActionValue& Value)
{
	float number = Value.Get<float>();

	// WeaponSlots go from 0, so we need number - 1
	TryToSwitchWeaponByKeyInput((int32)number - 1);
}

void ATDS_ViktoriaCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	myPC = Cast<APlayerController>(GetController());

	//Get the local player subsystem
	UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(myPC->GetLocalPlayer());
	
	//Clear existing mappings and add our mapping
	Subsystem->ClearAllMappings();
	Subsystem->AddMappingContext(InputMapping, 0);

	//Get enhanced input component
	UEnhancedInputComponent* PEI = Cast<UEnhancedInputComponent>(PlayerInputComponent);

	// Setup inputs
	PEI->BindAction(MoveInput, ETriggerEvent::Triggered, this, &ATDS_ViktoriaCharacter::Move);
	PEI->BindAction(FireInput, ETriggerEvent::Started, this, &ATDS_ViktoriaCharacter::InputAttackPressed);
	PEI->BindAction(FireInput, ETriggerEvent::Completed, this, &ATDS_ViktoriaCharacter::InputAttackReleased);
	PEI->BindAction(MagicInput, ETriggerEvent::Started, this, &ATDS_ViktoriaCharacter::CastMagic);
	PEI->BindAction(MagicInput, ETriggerEvent::Completed, this, &ATDS_ViktoriaCharacter::EndCastingMagic);
	PEI->BindAction(ReloadInput, ETriggerEvent::Started, this, &ATDS_ViktoriaCharacter::TryReloadWeapon);
	PEI->BindAction(SwitchNextWeapon, ETriggerEvent::Started, this, &ATDS_ViktoriaCharacter::TryToSwitchNextWeapon);
	PEI->BindAction(SwitchPreviousWeapon, ETriggerEvent::Started, this, &ATDS_ViktoriaCharacter::TryToSwitchPreviousWeapon);
	PEI->BindAction(AbilityInput, ETriggerEvent::Started, this, &ATDS_ViktoriaCharacter::TryExecuteAbility);
	PEI->BindAction(DropCurrentWeaponInput, ETriggerEvent::Started, this, &ATDS_ViktoriaCharacter::DropCurrentWeapon);
	PEI->BindAction(NumbersInput, ETriggerEvent::Started, this, &ATDS_ViktoriaCharacter::NumberInputPressed);

}

AWeaponDefault* ATDS_ViktoriaCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDS_ViktoriaCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo)
{
	// On Server
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					// Attach weapon
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));

					CurrentWeapon = myWeapon;

					// Set up weapon
					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->AdditionalWeaponInfo.Round = WeaponAdditionalInfo.Round;
					myWeapon->UpdateStateWeapon_OnServer(MovementState);
					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;

					// Set up weapon slot index
					if (InventoryComponent)
						CurrentWeaponIndex = InventoryComponent->GetWeaponIndexSlotByName(IdWeaponName);

					// Delegates
					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATDS_ViktoriaCharacter::WeaponFireStart);
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDS_ViktoriaCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDS_ViktoriaCharacter::WeaponReloadEnd);

					// for debug DELETE LATER
					myWeapon->ReloadTime = myWeapon->WeaponSetting.ReloadTime; 

					// After switch try reload if needed
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckWeaponCanReload())
						CurrentWeapon->InitReload();
				}
			}
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("ATDS_ViktoriaCharacter::InitWeapon - Weapon not found in table"));
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATDS_ViktoriaCharacter::InitWeapon - GameInstance is not subclass of UTDSGameInstance"));
}

void ATDS_ViktoriaCharacter::TryReloadWeapon()
{
	TryReloadWeapon_OnServer();
}

void ATDS_ViktoriaCharacter::AmmoChangeReloadInit(EWeaponType TypeAmmo, int32 Count, bool bTryReload)
{
	if (bTryReload && CurrentWeapon && CurrentWeapon->WeaponSetting.WeaponType == TypeAmmo)
		CurrentWeapon->InitReload();
}

void ATDS_ViktoriaCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentWeaponIndex, CurrentWeapon->AdditionalWeaponInfo);
	WeaponFireStart_BP(Anim);
}

void ATDS_ViktoriaCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATDS_ViktoriaCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTaken)
{
	if (bIsSuccess && InventoryComponent)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentWeaponIndex, CurrentWeapon->AdditionalWeaponInfo);
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, -AmmoTaken);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

void ATDS_ViktoriaCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATDS_ViktoriaCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATDS_ViktoriaCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// in BP
}


void ATDS_ViktoriaCharacter::MovementTick(float DeltaTime)
{
	if (HealthComponent->bPawnInControl)
	{
		if (GetController() && GetController()->IsLocalPlayerController())
		{
			FString SEnum = UEnum::GetValueAsString(MovementState);

			APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			if (MyController)
			{
				FVector MouseLocation;
				FVector MouseDirection;
				MyController->DeprojectMousePositionToWorld(MouseLocation, MouseDirection);

				// Find Intersection between line from pawn to cursor and plane 
				float T;
				FVector LineEnd = (MouseDirection * 10000) + MouseLocation;
				UKismetMathLibrary::LinePlaneIntersection_OriginNormal(MouseLocation, LineEnd, FVector(0, 0, 0), FVector(0, 0, 1), T, CursorPlaneIntersectionLocation);

				float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), CursorPlaneIntersectionLocation).Yaw;
				if (!SprintRunEnabled)
				{
					SetActorRotationByYaw_OnServer(FindRotatorResultYaw);
					//SetActorRotation(FRotator(0.0f, FindRotatorResultYaw, 0.0f));
				}

				if (CurrentWeapon)
				{
					FVector Displacement = FVector(0);
					bool bReduceDispersion = false;
					switch (MovementState)
					{
					case EMovementState::Aim_State:
						Displacement = FVector(0.0f, 0.0f, 160.0f);
						//CurrentWeapon->bShouldReduceDispersion = true;
						bReduceDispersion = true;
						break;
					case EMovementState::AimWalk_State:
						Displacement = FVector(0.0f, 0.0f, 160.0f);
						//CurrentWeapon->bShouldReduceDispersion = true;
						bReduceDispersion = true;
						break;
					case EMovementState::Walk_State:
						Displacement = FVector(0.0f, 0.0f, 120.0f);
						//CurrentWeapon->bShouldReduceDispersion = false;
						bReduceDispersion = true;
						break;
					case EMovementState::Run_State:
						Displacement = FVector(0.0f, 0.0f, 120.0f);
						//CurrentWeapon->bShouldReduceDispersion = false;
						bReduceDispersion = true;
						break;
					case EMovementState::SprintRun_State:
						break;
					default:
						break;
					}
					FHitResult HitResult;
					MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, HitResult);
					//CurrentWeapon->ShootEndLocation = FVector(HitResult.Location + Displacement);

					CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(HitResult.Location + Displacement, bReduceDispersion);
				}
			}
		}
	}
}

void ATDS_ViktoriaCharacter::EnergyTick(float DeltaTime)
{
	if (SprintRunEnabled && SprintEnergy > 0)
	{
		if (SprintEnergy >= DeltaTime * SprintCost)
		{
			SprintEnergy -= (DeltaTime * SprintCost);
			OnEnergyChange.Broadcast(SprintEnergy);
		}
		else
		{
			SprintEnergy = 0;
			SprintRunEnabled = false;
			ChangeMovementState();
		}

	}
	else if (SprintEnergy < SprintMaxEnergy)
	{
		SprintEnergy += (DeltaTime * SprintRefillStep);
		OnEnergyChange.Broadcast(SprintEnergy);
	}
}

void ATDS_ViktoriaCharacter::Move(const FInputActionValue& Value)
{
	if (HealthComponent->bPawnInControl)
	{
		const FVector2D MoveValue = Value.Get<FVector2D>();
		const FRotator MovementRotation(0, GetControlRotation().Yaw, 0);

		// Forward/Backward direction
		if (MoveValue.Y != 0.f)
		{
			// Get forward vector
			const FVector Direction = MovementRotation.RotateVector(FVector::ForwardVector);

			AddMovementInput(Direction, MoveValue.Y);
		}

		// Right/Left direction
		if (MoveValue.X != 0.f)
		{
			// Get right vector
			const FVector Direction = MovementRotation.RotateVector(FVector::RightVector);

			AddMovementInput(Direction, MoveValue.X);
		}
	}
}

void ATDS_ViktoriaCharacter::InputAttackPressed()
{
	if (HealthComponent->bPawnInControl)
		AttackCharEvent(true);
}

void ATDS_ViktoriaCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

bool ATDS_ViktoriaCharacter::CheckCanSprint()
{
	return (SprintEnergy >= SprintMinEnergyForSprintInit);
}

void ATDS_ViktoriaCharacter::CharacterUpdate()
{
	float ResultSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResultSpeed = MovementInfo.AimSpeedNormal;
		break;
	case EMovementState::Walk_State:
		ResultSpeed = MovementInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResultSpeed = MovementInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResultSpeed = MovementInfo.RunSpeedSprint;
		break;
	case EMovementState::AimWalk_State:
		ResultSpeed = MovementInfo.AimSpeedWalk;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;
}

void ATDS_ViktoriaCharacter::ChangeMovementState()
{
	EMovementState NewState = EMovementState::Run_State;
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		NewState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			NewState = EMovementState::SprintRun_State;
		}
		if (WalkEnabled && !SprintRunEnabled && AimEnabled)
		{
			NewState = EMovementState::AimWalk_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
			{
				NewState = EMovementState::Walk_State;
			}
			else
			{
				if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
				{
					NewState = EMovementState::Aim_State;
				}
			}
		}
	}
	SetMovementState_OnServer(NewState);

	//CharacterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon_OnServer(NewState);
	}
}

void ATDS_ViktoriaCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATDS_ViktoriaCharacter::AttackCharEvent - CurrentWeapon - NULL"));
}

void ATDS_ViktoriaCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		InventoryComponent->DropWeaponByIndex_OnServer(CurrentWeaponIndex);
	}
}

bool ATDS_ViktoriaCharacter::TryToSwitchWeaponByKeyInput(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (InventoryComponent)
	{
		if (CurrentWeaponIndex != ToIndex && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
		{
			FAdditionalWeaponInfo OldInfo;
			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
				if (CurrentWeapon->bWeaponReloading)
					CurrentWeapon->CancelReload();
			}

			bIsSuccess = InventoryComponent->SwitchWeaponToIndex(ToIndex, CurrentWeaponIndex);
		}
	}
	return bIsSuccess;
}

void ATDS_ViktoriaCharacter::InteractOverlapChanged_Implementation(bool bCanInteractState, AActor* PickUpActor)
{
}

void ATDS_ViktoriaCharacter::InteractSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!bCanInteract)
	{
		bCanInteract = true;
		InteractOverlapChanged(bCanInteract, OtherActor);
	}
}

void ATDS_ViktoriaCharacter::InteractSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	// Check if there is other interaction overlaps
	TArray<AActor*> OverlappingActorsArray;
	InteractSphere->GetOverlappingActors(OverlappingActorsArray);

	bool bOtherInteractorsDetected = false;
	AActor* OtherInteractor = nullptr;

	for (auto elem : OverlappingActorsArray)
	{
		if (!(elem == OtherActor))
		{
			bOtherInteractorsDetected = true;
			OtherInteractor = elem;
		}
	}

	if (bOtherInteractorsDetected)
		InteractOverlapChanged(bCanInteract, OtherInteractor);
	else
	{
		bCanInteract = false;
		InteractOverlapChanged(bCanInteract, OtherActor);
	}
}

void ATDS_ViktoriaCharacter::CastMagic()
{
	if (HealthComponent->bPawnInControl)
	{
		TSet<AActor*> OverlappingActorsSet;
		GetOverlappingActors(OverlappingActorsSet);
		for (auto& Elem : OverlappingActorsSet)
		{
			MagicObject = Cast<IMagicInterface>(Elem);
			if (MagicObject)
			{
				MagicObject->SpellCast();
				break;
			}
		}
	}
}

void ATDS_ViktoriaCharacter::EndCastingMagic()
{
	if (MagicObject)
	{
		MagicObject->SpellEndCasting();
	}
}


UDecalComponent* ATDS_ViktoriaCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

// Switching weapon next
void ATDS_ViktoriaCharacter::TryToSwitchNextWeapon()
{
	if (HealthComponent->bPawnInControl)
		TryToSwitchWeapon(true);
}

// Switching weapon previous
void ATDS_ViktoriaCharacter::TryToSwitchPreviousWeapon()
{
	if (HealthComponent->bPawnInControl)
		TryToSwitchWeapon(false);
}

void ATDS_ViktoriaCharacter::TryToSwitchWeapon(bool bIsForward)
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more than one weapon go switch
		if (CurrentWeapon)
		{
			if (CurrentWeapon->bWeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponIndexPush(CurrentWeaponIndex, bIsForward, CurrentWeaponIndex))
			{
			}
		}
	}
}

void ATDS_ViktoriaCharacter::OnDead()
{
	if (HasAuthority())
	{
		ITDS_IGameActor::OnDead();

		float AnimTime = 0.0f;
		int32 rnd = FMath::RandHelper(DeathAnims.Num());
		if (DeathAnims.IsValidIndex(rnd) && DeathAnims[rnd] && GetMesh())
		{
			if (GetMesh() && GetMesh()->HasValidAnimationInstance())
			{
				AnimTime = DeathAnims[rnd]->GetPlayLength();
				PlayAnimation_Multicast(DeathAnims[rnd]);
			}
			else
				UE_LOG(LogTemp, Error, TEXT("ATDS_ViktoriaCharacter::CharDead - CharacterMesh anim instance -NULL"));
		}

		if (GetController())
			GetController()->UnPossess();

		// RagDoll Timer
		GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATDS_ViktoriaCharacter::EnableRagDoll_Multicast, AnimTime);
	}
	else
	{
		// Hide cursor
		if (GetCursorToWorld())
		{
			GetCursorToWorld()->SetVisibility(false);
		}


		// Cancel fire
		AttackCharEvent(false);


	}

	if (GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
	}

	// For BP
	CharDead_BP();
}


void ATDS_ViktoriaCharacter::EnableRagDoll_Multicast_Implementation()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

void ATDS_ViktoriaCharacter::CharDead_BP_Implementation()
{
	//BP
}

bool ATDS_ViktoriaCharacter::GetSurfaceType(EPhysicalSurface& SurfaceType)
{
	bool result = false;
	if (HealthComponent)
	{
		if (HealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					SurfaceType = myMaterial->GetPhysicalMaterial()->SurfaceType;
					result = true;
				}
			}
		}
		else
		{
			SurfaceType = EPhysicalSurface::SurfaceType4;
			result = true;
		}
	}
	return result;
}

TArray<UTDS_StateEffect*> ATDS_ViktoriaCharacter::GetCurrentEffects()
{
	return Effects;
}

void ATDS_ViktoriaCharacter::RemoveEffect(UTDS_StateEffect* RemovedEffect)
{
	Effects.Remove(RemovedEffect);

	SwitchEffect(RemovedEffect, false);
	EffectRemove = RemovedEffect;
}

void ATDS_ViktoriaCharacter::AddEffect(UTDS_StateEffect* AddedEffect, float Duration)
{
	Effects.Add(AddedEffect);

	if (Duration > 0)
	{
		int32 EffectIndex = Effects.Find(AddedEffect);
		OnEffectAdded.Broadcast(EffectIndex, AddedEffect->EffectIcon, Duration);
	}

	SwitchEffect(AddedEffect, true);
	EffectAdd = AddedEffect;
}

void ATDS_ViktoriaCharacter::OnStunned()
{
	
	AttackCharEvent(false);
	if (CurrentWeapon)
	{
		if (CurrentWeapon->bWeaponReloading)
			CurrentWeapon->CancelReload();
	}
	HealthComponent->bPawnInControl = false;
	if (GetMesh()->HasValidAnimationInstance())
		GetMesh()->GetAnimInstance()->Montage_Play(StunAnimation);

	//myPC = Cast<APlayerController>(GetController());
	//DisableInput(myPC);
}

void ATDS_ViktoriaCharacter::StunEnd()
{
	HealthComponent->bPawnInControl = true;
	if (GetMesh()->HasValidAnimationInstance())
		GetMesh()->GetAnimInstance()->Montage_Stop(0.5f, StunAnimation);
}

void ATDS_ViktoriaCharacter::TryExecuteAbility()
{
	if (CanUseAbility && AbilityEffect && HealthComponent->bPawnInControl)
	{
		UTDS_StateEffect* NewEffect = NewObject<UTDS_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
			NewEffect->OnDestroyed.AddDynamic(this, &ATDS_ViktoriaCharacter::AbilityEnded);
			CanUseAbility = false;
		}
	}
}

void ATDS_ViktoriaCharacter::AbilityEnded()
{
	CanUseAbility = true;
}

void ATDS_ViktoriaCharacter::SetMovementState_Multicast_Implementation(EMovementState newState)
{
	MovementState = newState;
	CharacterUpdate();
}

void ATDS_ViktoriaCharacter::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void ATDS_ViktoriaCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void ATDS_ViktoriaCharacter::SwitchEffect(UTDS_StateEffect* Effect, bool IsAdded)
{
	UTDS_StateEffect_WithDuration* EffectTimer = Cast<UTDS_StateEffect_WithDuration>(Effect);
	if (EffectTimer && EffectTimer->DurationEffect)
	{
		if (IsAdded)
		{
			USkeletalMeshComponent* myMesh = GetMesh();
			if (myMesh)
			{
				UNiagaraComponent* myParticle = UNiagaraFunctionLibrary::SpawnSystemAttached(EffectTimer->DurationEffect, myMesh, Effect->NameBone, FVector(0), FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(myParticle);
			}
		}
		else
		{
			for (UNiagaraComponent* elem : ParticleSystemEffects)
			{
				if (elem->GetAsset() && EffectTimer->DurationEffect == elem->GetAsset())
				{
					elem->Deactivate();
					elem->DestroyComponent();
					break;
				}
			}
		}
	}
}

void ATDS_ViktoriaCharacter::PlayAnimation_Multicast_Implementation(UAnimMontage* AnimToPlay)
{
	if (GetMesh() && GetMesh()->HasValidAnimationInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(AnimToPlay);
	}
}

void ATDS_ViktoriaCharacter::TryReloadWeapon_OnServer_Implementation()
{
	if (CurrentWeapon && !CurrentWeapon->bWeaponAiming && HealthComponent->bPawnInControl)
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckWeaponCanReload())
		{
			CurrentWeapon->InitReload();
		}
	}
}

void ATDS_ViktoriaCharacter::SetMovementState_OnServer_Implementation(EMovementState newState)
{
	SetMovementState_Multicast(newState);
}

void ATDS_ViktoriaCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	//if (GetController() && !GetController()->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void ATDS_ViktoriaCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}

bool ATDS_ViktoriaCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);
	
	for (int i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}

	return Wrote;
}

void ATDS_ViktoriaCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATDS_ViktoriaCharacter, MovementState);
	DOREPLIFETIME(ATDS_ViktoriaCharacter, CurrentWeapon);
	DOREPLIFETIME(ATDS_ViktoriaCharacter, CurrentWeaponIndex);
	DOREPLIFETIME(ATDS_ViktoriaCharacter, Effects);
	DOREPLIFETIME(ATDS_ViktoriaCharacter, EffectAdd);
	DOREPLIFETIME(ATDS_ViktoriaCharacter, EffectRemove);
}


