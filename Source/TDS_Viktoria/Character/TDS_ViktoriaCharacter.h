// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS_Viktoria/Weapon/WeaponDefault.h"
#include "TDS_Viktoria/Interface/TDS_IGameActor.h"

#include "InputActionValue.h"

#include "TDS_Viktoria/FuncLibrary/GlobalTypes.h"
#include "TDS_ViktoriaCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEnergyChange, float, CurrentEnergy);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnEffectAdded, int32, EffectIndex, UTexture2D*, EffectIcon, float, EffectTime);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEffectRemoved, int32, EffectIndex);

UCLASS(Blueprintable)
class ATDS_ViktoriaCharacter : public ACharacter, public ITDS_IGameActor
{
	GENERATED_BODY()

public:
	ATDS_ViktoriaCharacter();

	virtual void Tick(float DeltaSeconds) override; // Called every frame.
	virtual void BeginPlay() override; // Called in beggining of the game
	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override; // For replication

	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Enhanced Input")
	class UInputMappingContext* InputMapping;

	//Input
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* MoveInput;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* FireInput;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* ReloadInput;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* MagicInput;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* SwitchNextWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* SwitchPreviousWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* AbilityInput;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* DropCurrentWeaponInput;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* NumbersInput;

	void NumberInputPressed(const FInputActionValue& Value);

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	//Components
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* InteractSphere;

	//Player Controller
	APlayerController* myPC = nullptr;

public:
	// Inventory
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory")
	class UTDSInventoryComponent* InventoryComponent;
	UPROPERTY(Replicated, BlueprintReadOnly, EditDefaultsOnly)
	int32 CurrentWeaponIndex = 0;

	// Health and Death
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health")
	class UTDSCharHealthComponent* HealthComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	TArray<UAnimMontage*> DeathAnims;
	FTimerHandle TimerHandle_RagDollTimer;


	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Cursor")
	FVector CursorPlaneIntersectionLocation = FVector(0);

	UDecalComponent* CurrentCursor = nullptr;

	// Movement mode info
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Movement")
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;

	// Movement mode bools
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool AimEnabled = false;

	// Sprint logic
	UPROPERTY(BlueprintReadOnly, Category = "Energy")
	float SprintEnergy;
	UPROPERTY(BlueprintReadOnly, Category = "Energy")
	float SprintMaxEnergy = 30;
	UPROPERTY(BlueprintReadOnly, Category = "Energy")
	float SprintMinEnergyForSprintInit = 5;
	float SprintCost = 10;
	float SprintRefillStep = 1;
	UPROPERTY(BlueprintAssignable, Category = "Energy")
	FOnEnergyChange OnEnergyChange;
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Energy")
	bool CheckCanSprint();

	// Weapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FName InitWeaponName;
	UPROPERTY(Replicated)
	AWeaponDefault* CurrentWeapon = nullptr;

	//Magic
	class IMagicInterface* MagicObject;

	// Abilities
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TSubclassOf<UTDS_StateEffect> AbilityEffect;

	// Effects
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects")
	UAnimMontage* StunAnimation = nullptr;
	UPROPERTY(BlueprintAssignable, Category = "EnEffectsergy")
	FOnEffectAdded OnEffectAdded;
	UPROPERTY(BlueprintAssignable, Category = "EnEffectsergy")
	FOnEffectRemoved OnEffectRemoved;

	/////// Functions /////////
	
	//Movement
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();

	void MovementTick(float DeltaTime);
	void EnergyTick(float DeltaTime);
	void Move(const FInputActionValue& Value);

	// Attack	
	void InputAttackPressed();
	void InputAttackReleased();

	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);

	// Weapon	
	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo);
	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();
	UFUNCTION()
	void AmmoChangeReloadInit(EWeaponType TypeAmmo, int32 Count, bool bTryReload);
	UFUNCTION()
	void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoTaken);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess);
	void DropCurrentWeapon();
	bool TryToSwitchWeaponByKeyInput(int32 ToIndex);

	// Interaction
	bool bCanInteract = false;
	UFUNCTION(BlueprintNativeEvent, Category = "Interact")
	void InteractOverlapChanged(bool bCanInteractState, AActor* PickUpActor);

	UFUNCTION()
	void InteractSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void InteractSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);


	// Magic
	void CastMagic();
	void EndCastingMagic();

	// Cursor
	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();

	// Inventory
	void TryToSwitchNextWeapon();
	void TryToSwitchPreviousWeapon();
	void TryToSwitchWeapon(bool bIsForward);

	// From ITDS_GameActor interface
	bool GetSurfaceType(EPhysicalSurface& SurfaceType) override;
	TArray<UTDS_StateEffect*> GetCurrentEffects() override;
	void RemoveEffect(class UTDS_StateEffect* RemovedEffect) override;
	void AddEffect(class UTDS_StateEffect* AddedEffect, float Duration = -1.0f) override;
	void OnStunned() override;
	void StunEnd() override;

	// Health and Death
	void OnDead() override; // from ITDS_GameActor
	UFUNCTION(NetMulticast, Reliable)
	void EnableRagDoll_Multicast();
	UFUNCTION(BlueprintNativeEvent, Category = "Health")
	void CharDead_BP();

	// Effects
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Effects")
	TArray<UTDS_StateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UTDS_StateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UTDS_StateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<class UNiagaraComponent*> ParticleSystemEffects;

	// Abilities
	void TryExecuteAbility();
	bool CanUseAbility = true;
	UFUNCTION()
	void AbilityEnded();

	// Network
	UFUNCTION(Server, Unreliable)
	void SetActorRotationByYaw_OnServer(float Yaw);
	void SetActorRotationByYaw_OnServer_Implementation(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotationByYaw_Multicast(float Yaw);
	void SetActorRotationByYaw_Multicast_Implementation(float Yaw);

	UFUNCTION(Server, Reliable)
	void SetMovementState_OnServer(EMovementState newState);
	void SetMovementState_OnServer_Implementation(EMovementState newState);
	UFUNCTION(NetMulticast, Reliable)
	void SetMovementState_Multicast(EMovementState newState);
	void SetMovementState_Multicast_Implementation(EMovementState newState);

	UFUNCTION(Server, Reliable)
	void TryReloadWeapon_OnServer();

	UFUNCTION(NetMulticast, Reliable, Category = "Animation")
	void PlayAnimation_Multicast(UAnimMontage* AnimToPlay);

	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION()
	void EffectRemove_OnRep();

	UFUNCTION()
	void SwitchEffect(UTDS_StateEffect* Effect, bool IsAdded);
};
