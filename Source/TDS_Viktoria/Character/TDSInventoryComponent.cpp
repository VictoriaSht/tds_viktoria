// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSInventoryComponent.h"

#include "TDS_Viktoria/Game/TDSGameInstance.h"
#include "TDS_Viktoria/Interface/TDS_IGameActor.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UTDSInventoryComponent::UTDSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// Network
	SetIsReplicatedByDefault(true);
}


// Called when the game starts
void UTDSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UTDSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTDSInventoryComponent::InitInventory_OnServer_Implementation(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoSlotsInfo)
{
	WeaponSlots = NewWeaponSlotsInfo;
	AmmoSlots = NewAmmoSlotsInfo;

	// Set MaxSlots Weapon
	MaxSlotsWeapon = WeaponSlots.Num();

	// First Init Weapon
	bool bWeaponFound = false;
	for (auto elem : WeaponSlots)
	{
		if (!elem.NameItem.IsNone())
		{
			SwitchWeaponEvent_Multicast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo);
			bWeaponFound = true;
		}
	}
	if (!bWeaponFound)
		UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::InitInventory - WeaponSlots empty"));
	
}

bool UTDSInventoryComponent::SwitchWeaponIndexPush(int32 CurrentIndex, bool bIsForward, int32& Newindex)
{
	int32 OldIndex = CurrentIndex;
	FAdditionalWeaponInfo OldInfo = WeaponSlots[CurrentIndex].AdditionalInfo;

	int8 CorrectIndex;
		if (bIsForward)
			CorrectIndex = CurrentIndex + 1;
		else
			CorrectIndex = CurrentIndex - 1;

	bool bIsSuccess = false;

	if (CurrentIndex > WeaponSlots.Num() - 1)
		CorrectIndex = 0;
	else if (CurrentIndex < 0)
		CorrectIndex = WeaponSlots.Num() - 1;

	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalWeaponInfo;

	///////////////////////////////////////
	int32 CurrentCheckedIndex;
	bool bEndCycle = false;
	int8 i = CorrectIndex;

	while (!bEndCycle)
	{
		CurrentCheckedIndex = i;

		if (WeaponSlots.IsValidIndex(CurrentCheckedIndex))
		{
			if (!WeaponSlots[CurrentCheckedIndex].NameItem.IsNone())
			{
				if (GetAmmoForWeapon(WeaponSlots[CurrentCheckedIndex].WeaponType) > 0 || WeaponSlots[CurrentCheckedIndex].AdditionalInfo.Round > 0)
				{
					NewIdWeapon = WeaponSlots[CurrentCheckedIndex].NameItem;
					NewAdditionalWeaponInfo = WeaponSlots[CurrentCheckedIndex].AdditionalInfo;
					CorrectIndex = CurrentCheckedIndex;
					bIsSuccess = true;
					bEndCycle = true;
				}

			}
		}
		else
		{
			if (bIsForward)
			{
				i = -1;
				CorrectIndex = -1;
			}
			else
			{
				i = WeaponSlots.Num();
				CorrectIndex = WeaponSlots.Num();
			}
		}
		if (bIsForward)
			i++;
		else
			i--;
	}
	///////////////////////////////////////

	if (!bIsSuccess)
	{

	}

	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex, OldInfo);
		SwitchWeaponEvent_Multicast(NewIdWeapon, NewAdditionalWeaponInfo);
	}

	Newindex = CorrectIndex;
	return bIsSuccess;
}

bool UTDSInventoryComponent::SwitchWeaponToIndex(int32 NewIndex, int32 CurrentIndex)
{
	int32 OldIndex = CurrentIndex;
	FAdditionalWeaponInfo OldInfo = WeaponSlots[CurrentIndex].AdditionalInfo;

	bool bIsSuccess = false;
	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalWeaponInfo;

	if (WeaponSlots.IsValidIndex(NewIndex) && !WeaponSlots[NewIndex].NameItem.IsNone())
	{
		if (GetAmmoForWeapon(WeaponSlots[NewIndex].WeaponType) > 0 || WeaponSlots[NewIndex].AdditionalInfo.Round > 0)
		{
			NewIdWeapon = WeaponSlots[NewIndex].NameItem;
			NewAdditionalWeaponInfo = WeaponSlots[NewIndex].AdditionalInfo;
			bIsSuccess = true;
		}
	}

	if (!bIsSuccess)
	{

	}

	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex, OldInfo);
		SwitchWeaponEvent_Multicast(NewIdWeapon, NewAdditionalWeaponInfo);
	}

	return bIsSuccess;
}

FAdditionalWeaponInfo UTDSInventoryComponent::GetAdditionalInfoWeapon(int32 WeaponIndex)
{
	FAdditionalWeaponInfo result;
	if (WeaponSlots.IsValidIndex(WeaponIndex))
		result = WeaponSlots[WeaponIndex].AdditionalInfo;
	else
		UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::GetAdditionalInfoWeapon - Not correct index weapon %d"), WeaponIndex)

	return result;
}

int32 UTDSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFound = false;
	while (i < WeaponSlots.Num() && !bIsFound)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFound = true;
			result = i;
		}
		i++;
	}

	return result;
}

FName UTDSInventoryComponent::GetWeaponNameBySlot(int32 IndexSlot)
{
	FName result = NAME_None;

	if (WeaponSlots.IsValidIndex(IndexSlot))
		result = WeaponSlots[IndexSlot].NameItem;
	else
		UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::GetWeaponNameBySlot - not valid index"));

	return result;
}

void UTDSInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		WeaponSlots[IndexWeapon].AdditionalInfo = NewInfo;
		WeaponChangeEvent_Multicast(IndexWeapon, WeaponSlots[IndexWeapon]);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::SetAdditionalInfoWeapon - Not correct index weapon %d"), IndexWeapon);

}

void UTDSInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 AmmoAdded)
{
	bool bWeaponTryReload = true;

	bool bIsFound = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFound)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			if (AmmoSlots[i].Count > 0)
				bWeaponTryReload = false;

			AmmoSlots[i].Count += AmmoAdded;
			if (AmmoSlots[i].Count > AmmoSlots[i].MaxCount)
				AmmoSlots[i].Count = AmmoSlots[i].MaxCount;

			AmmoChangeEvent_Multicast(AmmoSlots[i].WeaponType, AmmoSlots[i].Count, bWeaponTryReload);

			bIsFound = true;
		}
		i++;
	}
}

int32 UTDSInventoryComponent::GetAmmoForWeapon(EWeaponType WeaponType)
{
	int32 result = 0;
	int8 i = 0;
	while (i < AmmoSlots.Num())
	{
		if (AmmoSlots[i].WeaponType == WeaponType)
		{
			if (AmmoSlots[i].Count <= 0)
				AmmoChangeEvent_Multicast(WeaponType, AmmoSlots[i].Count, false);
			result = AmmoSlots[i].Count;
			break;
		}
		i++;
	}
	return result;
}

bool UTDSInventoryComponent::HaveEmptyWeaponSlots()
{
	bool result = false;

	for (auto elem : WeaponSlots)
	{
		if (elem.NameItem.IsNone())
		{
			result = true;
			break;
		}
	}
	return result;
}

bool UTDSInventoryComponent::CheckCanTakeAmmo(EWeaponType WeaponType)
{
	bool result = false;
	int8 i = 0;
	while (i < AmmoSlots.Num())
	{
		if (AmmoSlots[i].WeaponType == WeaponType && AmmoSlots[i].Count < AmmoSlots[i].MaxCount)
			result = true;
		i++;
	}

	return result;
}

bool UTDSInventoryComponent::CheckCanTakeWeapon(int32& FreeSlot, FName NewWeaponId)
{
	bool bHaveSameWeapon = false;
	bool bHaveFreeSlot = false;
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bHaveSameWeapon) // we stop the loop if there is same weapon
	{
		// Default NewWeaponId == NAME_None
		if (WeaponSlots[i].NameItem == NewWeaponId)
		{
			bHaveSameWeapon = true;
			FreeSlot = i; // even if there was saved free slot, it will be rewrite by this index
		}
		if (!bHaveSameWeapon && !bHaveFreeSlot && WeaponSlots[i].NameItem.IsNone()) // if there is no same weapon, we check for the first free slot
		{
			bHaveFreeSlot = true;
			FreeSlot = i;
		}
		i++;
	}

	if (bHaveFreeSlot || bHaveSameWeapon)
		return true;
	else
		return false;
}

bool UTDSInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem& DropItemInfo, FWeaponSlot& OldWeapon)
{
	bool bIsSuccess = false;

	// Memorize old data
	int32 OldSlotIndex = IndexSlot;
	OldWeapon = WeaponSlots[OldSlotIndex];
	FName OldWeaponName = OldWeapon.NameItem;

	if (WeaponSlots.IsValidIndex(IndexSlot))
	{
		WeaponSlots[IndexSlot] = NewWeapon;
		if (SwitchWeaponToIndex(IndexSlot, CurrentIndexWeaponChar))
			bIsSuccess = true;
		else
		{
			int32 NewIndex;
			if (SwitchWeaponIndexPush(CurrentIndexWeaponChar, true, NewIndex))
				bIsSuccess = true;
			else
				UE_LOG(LogTemp, Error, TEXT("UTDSInventoryComponent::SwitchWeaponToInventory - can't switch"));
		}
	}

	if (bIsSuccess)
	{
		WeaponChangeEvent_Multicast(OldSlotIndex, OldWeapon);
		WeaponChangeEvent_Multicast(IndexSlot, NewWeapon);

		// Get Drop Item Info
		FName DropItemName = GetWeaponNameBySlot(IndexSlot);
		UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
		if (myGI)
		{
			FWeaponInfo WeaponInfo;
			if (myGI->GetWeaponInfoByName(OldWeaponName, WeaponInfo))
				myGI->GetDropItemInfoByName(WeaponInfo.DropItemNameId, DropItemInfo);
		}
	}

	return bIsSuccess;
}

void UTDSInventoryComponent::TryGetWeaponToInventory_OnServer_Implementation(AActor* PickUpActor, FWeaponSlot NewWeapon)
{
	bool bIsSuccess = false;
	int32 IndexSlot = -1;
	if (CheckCanTakeWeapon(IndexSlot, NewWeapon.NameItem))
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			WeaponSlots[IndexSlot] = NewWeapon;
			WeaponChangeEvent_Multicast(IndexSlot, NewWeapon);
			bIsSuccess = true;

			if (PickUpActor)
			{
				PickUpActor->Destroy();
			}
		}
	}
}

void UTDSInventoryComponent::DropWeaponByIndex_OnServer_Implementation(int32 Index)
{
	FDropItem DropItemInfo;
	FWeaponSlot EmptyWeaponSlot;

	// Check if there is other weapon, so we don't drop our last weapon
	bool bIsDroppable = false;
	int32 AvailableWeaponNum = 0;
	for (FWeaponSlot elem : WeaponSlots)
	{
		if (!elem.NameItem.IsNone())
		{
			AvailableWeaponNum++;
			if (AvailableWeaponNum > 1)
			{
				bIsDroppable = true;
				break;
			}
		}
	}

	if (bIsDroppable && WeaponSlots.IsValidIndex(Index))
	{
		if (GetDropItemInfoFromInventory(Index, DropItemInfo))
		{
			// Switch to first valid weapon in inventory from it's beginning
			int8 i = 0;
			while(i < WeaponSlots.Num())
			{
				if (i != Index && !WeaponSlots[i].NameItem.IsNone())
				{
					SwitchWeaponEvent_Multicast(WeaponSlots[i].NameItem, WeaponSlots[i].AdditionalInfo);
					break;
				}
				i++;
			}

			// Remember weapon info
			FWeaponSlot WeaponInfo = WeaponSlots[Index];

			// Empty slot at index
			WeaponSlots[Index] = EmptyWeaponSlot;

			// Drop weapon to world
			if (GetOwner()->GetClass()->ImplementsInterface(UTDS_IGameActor::StaticClass()))
				ITDS_IGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo, WeaponInfo);

			// Call delegate for widget update
			WeaponChangeEvent_Multicast(Index, EmptyWeaponSlot);
		}
	}

}

bool UTDSInventoryComponent::GetDropItemInfoFromInventory(int32 Index, FDropItem& DropItemInfo)
{
	bool result = false;

	FName DropItemName = GetWeaponNameBySlot(Index);
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		FWeaponInfo DropWeaponInfo;
		result = myGI->GetDropItemInfoByName(DropItemName, DropItemInfo);
		if (myGI->GetWeaponInfoByName(DropItemName, DropWeaponInfo))
			result = myGI->GetDropItemInfoByName(DropWeaponInfo.DropItemNameId, DropItemInfo);
	}

	return result;
}

void UTDSInventoryComponent::GetSlots(TArray<FWeaponSlot>& OutWeaponSlots, TArray<FAmmoSlot>& OutAmmoSlots)
{
	OutWeaponSlots = WeaponSlots;
	OutAmmoSlots = AmmoSlots;
}

void UTDSInventoryComponent::SwitchWeaponEvent_Multicast_Implementation(FName WeaponName, FAdditionalWeaponInfo WeaponAddInfo)
{
	OnSwitchWeapon.Broadcast(WeaponName, WeaponAddInfo);
}

void UTDSInventoryComponent::WeaponChangeEvent_Multicast_Implementation(int32 SlotIndex, FWeaponSlot NewInfo)
{
	OnUpdateWeaponSlot.Broadcast(SlotIndex, NewInfo);
}

void UTDSInventoryComponent::AmmoChangeEvent_Multicast_Implementation(EWeaponType TypeWeapon, int32 Count, bool bTryToReload)
{
	OnAmmoSlotInfoChange.Broadcast(TypeWeapon, Count, bTryToReload);
}



void UTDSInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTDSInventoryComponent, WeaponSlots);
	DOREPLIFETIME(UTDSInventoryComponent, AmmoSlots);
}