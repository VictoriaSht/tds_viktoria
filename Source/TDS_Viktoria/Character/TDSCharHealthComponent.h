// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSHealthComponent.h"
#include "TDSCharHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, CurrentShield, float, ChangeValue);

/**
 * 
 */
UCLASS()
class TDS_VIKTORIA_API UTDSCharHealthComponent : public UTDSHealthComponent
{
	GENERATED_BODY()

protected:
	float Shield = 100.0f;
	
public:
	UPROPERTY(BlueprintAssignable, Category = "Shield")
	FOnShieldChange OnShieldChange;

	void DecreaseHealth_OnServer(float Damage) override;


	// Shield cooldown
	FTimerHandle TimerHandle_ShieldRestoreCooldown;
	FTimerHandle TimerHandle_ShieldRestoreTick;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRestoreCooldownTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRestoreTickTime = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRestoreTickValuePerSecond = 10.0f;
	void ShieldRestoreCooldownEnd();
	void ShieldRestoreTick();


	UFUNCTION(BlueprintCallable, Category = "Shield")
	float GetCurrentShield();
	UFUNCTION(BlueprintCallable, Category = "Shield")
	void SetShield(float NewShield);

	// Damages shield. If shield destroyes by that - returns true
	UFUNCTION(BlueprintCallable, Category = "Shield")
	void TryDestroyShield(float Damage, bool GenerateShieldDestroyEvent = true);
	// Restores shield by value
	bool RestoreShield(float Restoration);
	void ShieldDestroyed();

	// Network
	UFUNCTION(NetMulticast, Reliable, Category = "Shield")
	void OnShieldChangeEvent_Multicast(float CurrentShield, float ChangeValue);
};
