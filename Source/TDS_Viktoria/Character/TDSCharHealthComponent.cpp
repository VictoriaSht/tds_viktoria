// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSCharHealthComponent.h"

void UTDSCharHealthComponent::DecreaseHealth_OnServer(float Damage)
{
	if (Shield > 0.0f)
	{
		Damage *= DamageCoef; // Change damage by coef
		TryDestroyShield(Damage);
	}
	else
	{
		Super::DecreaseHealth_OnServer(Damage);
	}
}

float UTDSCharHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTDSCharHealthComponent::SetShield(float NewShield)
{
	if (Shield > NewShield)
		TryDestroyShield(Shield - NewShield, false);
	else
		RestoreShield(NewShield - Shield);
}

void UTDSCharHealthComponent::TryDestroyShield(float Damage, bool GenerateShieldDestroyEvent)
{
	Shield -= Damage;
	if (Shield < 0.0f)
	{
		Shield = 0.0f;
		Damage += Shield; // Substract from shield value that went under zero
		if (GenerateShieldDestroyEvent)
			ShieldDestroyed();
	}
	
	OnShieldChangeEvent_Multicast(Shield, -Damage);

	// Start shield cooldown restoraion timer
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRestoreCooldown, this, &UTDSCharHealthComponent::ShieldRestoreCooldownEnd, ShieldRestoreCooldownTime);
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRestoreTick);
}

bool UTDSCharHealthComponent::RestoreShield(float Restoration)
{
	bool result = false;
	Shield += Restoration;
	if (Shield >= 100.0f)
	{
		Shield = 100.0f;
		Restoration -= 100 - Shield; // substract overshield
		result = true;
	}
	OnShieldChangeEvent_Multicast(Shield, Restoration);
	return result;
}

void UTDSCharHealthComponent::ShieldDestroyed()
{
	
}

void UTDSCharHealthComponent::OnShieldChangeEvent_Multicast_Implementation(float CurrentShield, float ChangeValue)
{
	OnShieldChange.Broadcast(CurrentShield, ChangeValue);
}

void UTDSCharHealthComponent::ShieldRestoreCooldownEnd()
{
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRestoreTick, this, &UTDSCharHealthComponent::ShieldRestoreTick, ShieldRestoreTickTime, true);
}

void UTDSCharHealthComponent::ShieldRestoreTick()
{
	if (RestoreShield(ShieldRestoreTickValuePerSecond * ShieldRestoreTickTime))
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRestoreTick);
}
